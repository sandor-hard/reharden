;	REHARDEN demo (introducing HARD Color Map "HCM" & HARDBASS)
;	Created by Sandor Teli / HARD (10/2016-11/2017)
;
;	requirements: 64K PAL XL/XE

	ICL "equates.asm"

	.GET "pic1.hcm"
	.GET [$2010] "pic2.hcm"
	.GET [$4020] "pic3.hcm"
	.GET [$6030] "hcmparts.dat"

	rmt_music_data = $d800
	
	rmt_player = PLAYER
	
	hcm_file_mode_descriptor_offset = 6
	hcm_file_palette_colors_offset = 7
	hcm_file_pic_data_start_offset = $800
	
	color_bck = 0
	color_underlay1 = 1
	color_underlay2 = 2
	color_playfield0 = 3
	color_playfield1 = 4
	color_playfield2 = 5

;	.def testcolors
;	.def debugtracker
;	.def debugtimers
;	.def debugstatemachine
	.def rmt
	.def readyanim

	timer = 4

	.ifndef debugtimers	
	sample_register = AUDC1+(timer-1)*2
	.else
	sample_register = COLBK
	.endif
	
	num_samples = 312>>3 ;# of scanlines/8

	cutoff_range = $f0
        cutoff_margin = (255-cutoff_range) >> 1
        cutoff_step = cutoff_range >> 4
        
        max_pulse_inc = 6

	dma_setting = 1+4+8+16+32

	middle_dlist_max_lines = $32 ;max=$54
	middle_display_data_lines = $10
	middle_display_data_line_bytes = $41

	pic3color = $90
	tscoloroffset = $b0

	ts_pf0 = $04
	ts_pf1 = $06
	ts_pf2 = $08
	ts_pf3 = $0a

	ts_refl_color = $80
	ts_refl_pf0 = $70
	ts_refl_pf1 = $72
	ts_refl_pf2 = $74
	ts_refl_pf3 = $76

	ts_player1_color = $02
	ts_player2_color = $00
	ts_player3_color = $02
	ts_player4_color = $00

	org $0200

init	sei

	ldx #$00
cpldl	lda loader_dlist_template,x
	sta loader_dlist,x
	inx
	cpx #(loader_dlist_template_end-loader_dlist_template)+1
	bne cpldl

	lda #$00
	ldx #$00
flscr	sta loader_screen,x
	inx
	cpx #80
	bne flscr

	lda #$80
	sta loader_screen+42

	lda #$22
	sta DMACTL

	lda #$00
	sta NMIEN
	sta IRQEN

	lda #$7c
initws	cmp VCOUNT
	bne initws

	lda #>loader_screen
	sta CHBASE ;all we need is an empty space character

	lda #$b2
	sta PORTB

	lda #<loader_dlist
	sta DLISTL
	lda #>loader_dlist
	sta DLISTH
	
	lda #$00
	sta COLBK
	lda #$CA
	sta COLPF1
	lda #$94
	sta COLPF2

	rts

loader_dlist_template
	.byte $70, $70, $70
	.byte $42, <(loader_screen+40), >(loader_screen+40)
	.rept 23
	.byte $42, <loader_screen, >loader_screen
	.endr
	.byte $41, <loader_dlist, >loader_dlist
loader_dlist_template_end


	ini init

	org $0200

doemp_jmp_lo
	.byte <doemp0, <doemp1, <doemp2, <doemp3, <doemp4, <doemp5, <doemp6, <doemp7
doemp_jmp_hi
	.byte >doemp0, >doemp1, >doemp2, >doemp3, >doemp4, >doemp5, >doemp6, >doemp7

middle_dlistjmptbl_lo
	.ds middle_dlist_max_lines
middle_dlistjmptbl_hi
	.ds middle_dlist_max_lines

middle_dlisttbl_lo_lo
	.ds middle_dlist_max_lines
middle_dlisttbl_lo_hi
	.ds middle_dlist_max_lines

rraddr_lo
	.byte <(rraddr1+1), <(rraddr3+1), <(rraddr5+1), <(rraddr7+1)
rraddr_hi
	.byte >(rraddr1+1), >(rraddr3+1), >(rraddr5+1), >(rraddr7+1)

	org $02e8
	;this must remain aligned to xxe8 so that the IRQ can read from the start of a page (+$18)

samplebuf
	.ds num_samples

middle_dlist
	.rept middle_dlist_max_lines
	.byte $5f, $00, $00
	.endr
	.byte $01
middle_dlist_jmp_addr
	.ds 2

stripe_char_pos
	.ds 28

stripe_char_speed_cnt
	.ds 28

unused
	.ds 8

	org samplebuf+$100
samplebuf2
	.ds num_samples

stillgoes_jmp_lo
	.byte <stillgoes0, <stillgoes1, <stillgoes2, <stillgoes3, <stillgoes4, <stillgoes5, <stillgoes6, <stillgoes7
stillgoes_jmp_hi
	.byte >stillgoes0, >stillgoes1, >stillgoes2, >stillgoes3, >stillgoes4, >stillgoes5, >stillgoes6, >stillgoes7

middle_display_data_lo
	.ds middle_display_data_lines
middle_display_data_hi
	.ds middle_display_data_lines

doempbgmap
	.byte 0, 0, 0, 2, 2, 4, 4, 4, 4, 8, 8, 8, 8, 8, 8

stripe_frame_delay
	.byte 0+5*0
	.byte 120
	.byte 2+5*3
	.byte 3+5*10
	.byte 120

	.byte 0+5*2
	.byte 1+5*0
	.byte 120
	.byte 3+5*15
	.byte 4+5*10

	.byte 0+5*1
	.byte 1+5*3
	.byte 2+5*15
	.byte 120
	.byte 120

	.byte 0+5*0
	.byte 120
	.byte 2+5*7
	.byte 3+5*5
	.byte 4

	.byte 0+5*8
	.byte 1+5*4
	.byte 120
	.byte 3+5*2
	.byte 120

	.byte 0+5*9
	.byte 1+5*3
	.byte 2+5*13

stripe_char_speed
	.byte 5
	.byte 3
	.byte 5+3
	.byte 3
	.byte 2

	.byte 5*2
	.byte 5*2
	.byte 5
	.byte 5+3
	.byte 5

	.byte 5*2
	.byte 5
	.byte 5+3
	.byte 6
	.byte 4

	.byte 5
	.byte 3
	.byte 5
	.byte 5
	.byte 5*2

	.byte 5+3
	.byte 5
	.byte 4
	.byte 5
	.byte 3

	.byte 5
	.byte 5*2
	.byte 5+3

times8
	.rept 32, #
	.byte :1*8
	.endr

dlistjmptbl_lo
	.ds $c0
dlistjmptbl_hi
	.ds $c0

filter_tbls
	.byte $f0, $e0, $d0, $c0, $b0, $a0, $90, $80, $70, $60, $50, $40, $30, $20, $10, $00

filter_dshim1s
	.ds 16

	.align $100
cutoff_scale_old .ds $100

	.align $800
	
	;we recycle pic1 data as the twister scroller screen
ts_sprites ;size = $800
	ts_screen = ts_sprites + $800 ;size = 48*10
	ts_screen2 = ts_screen + $200 ;size = 48*10


	hcmparts_length = $39d
pic1data
	.sav [$10] $800
	.sav [$6030] hcmparts_length ;the REHARDEN logo doesn't need this space - let's re-use it
	.sav [$10+$800+hcmparts_length] $1400-hcmparts_length

	hcmparts = pic1data+$800
	hcmpart1_length = $1bd
	hcmpart1 = hcmparts
	hcmpart2 = hcmparts+hcmpart1_length
	hcmpart2_sprite1 = hcmparts+$2fd;
	hcmpart2_sprite2 = hcmparts+$34d;

rrdata	INS "rrdata.dat"

	.align $100

pic2data
	.sav [$2020] $2000
pic3data
	.sav [$4030] $2000
	
sprite_start
	.ds $800
	sprite_replica_start = sprite_start
display
	.ds $1800

middle_display_mem
	.ds middle_display_data_lines*middle_display_data_line_bytes

after_display

	org sprite_start

readyanim_data
	INS "readyanim.dat"

	readyanim_char_data = readyanim_data
	readyanim_anim_data = readyanim_data+$200

	.align $400

loader_screen
	.ds 80

loader_dlist
	.ds (loader_dlist_template_end-loader_dlist_template)+1

readyanim_dlist
	.byte $77, <readyanim_screen, >readyanim_screen
	.rept 14, #
	.byte $77, <(readyanim_screen+(:1+1)*24), >(readyanim_screen+(:1+1)*24)
	.endr
	.byte $41, <readyanim_dlist, >readyanim_dlist

readyanim_bridge_fill_dlist
	.byte $70,$70,$70,$70,$70,$70,$70
	.rept 8, #
	.byte $47, <(readyanim_screen+:1*24), >(readyanim_screen+:1*24)
	.endr
	.byte $41, <readyanim_bridge_fill_dlist, >readyanim_bridge_fill_dlist
	
readyanim_screen
	.ds 15*24

readyanim_flicker_length
	.byte readyanim_flicker_which-readyanim_flicker_time

readyanim_flicker_time
	.byte 1,50,1,10,1,30,1,1,5,1,15,1

readyanim_flicker_which
	.byte 1,0,1,0,1,0,1,0,1,0,1,0

doreadyanimflicker
	ldx #$00
rafl0	jsr waitforvblank
	ldy readyanim_flicker_time,x
	lda readyanim_flicker_which,x
	beq rafl_ls

	jsr showreadyanimscreen
rafl_rs0
	lda #$04
	jsr waitforvcount
	sty rafl_rs1+1
	stx rafl_rs2+1
	ldx #$f0
	jsr dozigzag
rafl_rs2
	ldx #$00
rafl_rs1
	ldy #$00
	dey
	bne rafl_rs0
	jmp raflnext

rafl_ls
	
	jsr showloaderscreen
	lda #$04
rafl_ls0
	jsr waitforvcount
	sta WSYNC
	sta WSYNC
	dey
	bne rafl_ls0

raflnext
	inx
	cpx readyanim_flicker_length
	bne rafl0
	rts

doreadyanim

	lda #$00
	ldx #$00
dordfill
	sta readyanim_screen,x
	inx
	bne dordfill
dordfill2
	sta readyanim_screen+$100,x
	inx
	cpx #$68
	bne dordfill2

	lda #<readyanim_anim_data
	sta zp_tmp
	lda #>readyanim_anim_data
	sta zp_tmp+1

	jsr process_ready_anim
	jsr process_ready_anim

	jsr doreadyanimflicker

	jsr waitforvblank

	jsr initreadyanimsprites

	jsr showreadyanimscreen

readyanimloop
	jsr waitforvblank
		
	jsr process_ready_anim
	jsr addanimnoise
	inc addanimnoise+1
	
	lda #$04
	jsr waitforvcount

	ldx #$37
	jsr dozigzag
	sta WSYNC

	ldx #$10
	jsr dozigzag_withpm

	lda #$00
	sta GRAFP0
	sta GRAFP1
	sta GRAFP2
	sta GRAFP3

	ldx #$38
	jsr dozigzag

dordpmcc
	ldx #$00
	lda readyanim_pm_colors,x
	beq dordpmccend
	sta COLPM0
	sta COLPM1
	sta COLPM2
	sta COLPM3
	inc dordpmcc+1
dordpmccend

readyanimcnt
	lda #$00
	cmp #$c0
	beq readyanimbridge

	inc readyanimcnt+1

	jmp readyanimloop

readyanimbridge

	lda #<readyanim_bridge_fill_dlist
	sta DLISTL
	lda #>readyanim_bridge_fill_dlist
	sta DLISTH

	lda #$21
	sta DMACTL

	lda #$01
	sta readyanim_screen

	lda #$2c
	sta dozighs

	lda #$ea
	sta COLPF0
	lda #$00
	sta COLPF1
	sta COLPF2
	sta COLPF3
	lda #$01
	sta PRIOR
	
	ldx #$07
ranbr1	lda readyanimbridge_fill_lineaddrs_lo,x
	sta ranbr2+1
	lda readyanimbridge_fill_lineaddrs_hi,x
	sta ranbr2+2
	ldy #$02
	lda #$00
ranbr2	sta $ffff,y
	dey
	bpl ranbr2
	dex
	bpl ranbr1
	
ranbr0
	lda #$20
	jsr waitforvcount

	ldx #$10
	jsr dozigzag_withpm

	lda #$00
	sta GRAFP0
	sta GRAFP1
	sta GRAFP2
	sta GRAFP3

	ldx #$07
ranbr3	lda readyanimbridge_fill_lineaddrs_lo,x
	sta ranbr4+1
	lda readyanimbridge_fill_lineaddrs_hi,x
	sta ranbr4+2
	ldy readyanimbridge_fillpos,x
	bmi ranbrskipchar
	beq ranbrskipchar
	cpy #15
	beq ranbrskipinc
	
	lda #$01
ranbr4	sta $ffff,y
ranbrskipchar
	inc readyanimbridge_fillpos,x
ranbrskipinc
	dex
	bpl ranbr3

ranbrcnt
	ldx #$00
	inx
	cpx #$20
	beq readyanimend
	inc ranbrcnt+1
	jmp ranbr0

readyanimend
	jmp waitforvblank

readyanimbridge_fill_lineaddrs_lo
	.rept 8, #
	.byte <(readyanim_screen+:1*24)
	.endr
	
readyanimbridge_fill_lineaddrs_hi
	.rept 8, #
	.byte >(readyanim_screen+:1*24)
	.endr
	
readyanimbridge_fillpos
	.rept 4
	.byte $00,$f0
	.endr
	
dozigzag
dozig0	lda RANDOM
	and #$03
	ora #$08
	sta WSYNC
	nop
	nop
	nop
	nop
	nop
	nop
	sta HSCROL
	dex
	bne dozig0
	rts

readyanim_pm_colors
	.byte $86, $87, $88, $89, $8a, $9a, $aa, $ba, $ca, $da, $ea, $fa, $0a, $1a, $2a, $3a, $4a, $8a
	.byte 0

dozigzag_withpm
dozigp0
	lda #$55
	sta GRAFP0
	sta GRAFP1
	sta GRAFP2
	sta GRAFP3
	eor #$ff
	sta dozigp0+1

	sta WSYNC

	ldy #$07
dozigp1	lda RANDOM
	and #$03
	ora #$08
dozighs	sta HSCROL
	sta WSYNC
	dey
	bne dozigp1
	dex
	bne dozigp0
	rts

addanimnoise
	lda #$00
	asl
	tay
aano0

	lda RANDOM
	and #$c0
	ldx RANDOM
	ora readyanim_screen,x
	sta readyanim_screen,x

	lda RANDOM
	and #$c0
	ldx RANDOM
	ora readyanim_screen+$67,x
	sta readyanim_screen+$67,x
	
	dey
	bpl aano0
	rts

showloaderscreen
	lda #>loader_screen
	sta CHBASE ;all we need is an empty space character

	lda #<loader_dlist
	sta DLISTL
	lda #>loader_dlist
	sta DLISTH
	
	lda #$CA
	sta COLPF1
	lda #$94
	sta COLPF2

	lda #$22
	sta DMACTL
	rts

showreadyanimscreen
	lda #$00
	sta COLBK
	lda #$94
	sta COLPF0
	lda #$92
	sta COLPF1
	lda #$90
	sta COLPF2
	lda #$00
	sta COLPF3

	lda #<readyanim_dlist
	sta DLISTL
	lda #>readyanim_dlist
	sta DLISTH

	lda #>readyanim_char_data
	sta CHBASE

	lda #$23
	sta DMACTL

	lda #$0c
	sta HSCROL
	lda #$01
	sta VSCROL
	rts

showreadyanim_bridgescreen
	lda #$8a
	sta COLPF0
	lda #$ea
	sta COLPF1

	lda #<readyanim_bridge_dlist
	sta DLISTL
	lda #>readyanim_bridge_dlist
	sta DLISTH

	lda #$21
	sta DMACTL

	rts

process_ready_anim

	lda #<readyanim_screen
	sta zp_tmp2
	lda #>readyanim_screen
	sta zp_tmp2+1
	
	lda #$00
	sta zp_tmp9
praloop
	ldy zp_tmp9
	lda (zp_tmp),y
	bpl pravary

	cmp #$fd
	beq prajmp
	
	cmp #$fe
	beq praendfr

	cmp #$ff
	bne prarept
	jmp praendanim
	
prajmp	iny	
	lda (zp_tmp),y
	clc
	adc zp_tmp2
	sta zp_tmp2
	bcc prjmpnoinc
	inc zp_tmp2+1
prjmpnoinc
	lda zp_tmp9
	clc
	adc #$02
	sta zp_tmp9
	jmp praloop
	
prarept
	and #$7f
	sta zp_tmp6
	sta zp_tmp7

	inc zp_tmp9
	ldy zp_tmp9
	lda (zp_tmp),y
	ldy zp_tmp7
	dey
prarept0
	sta (zp_tmp2),y
	dey
	bpl prarept0
	lda zp_tmp2
	clc
	adc zp_tmp6
	sta zp_tmp2
	bcc prareptnoinc
	inc zp_tmp2+1
prareptnoinc
	inc zp_tmp9
	jmp praloop

pravary
	inc zp_tmp9
	sta zp_tmp6
	ldy #$00
	sty zp_tmp7
pravary0
	ldy zp_tmp9
	lda (zp_tmp),y
	ldy zp_tmp7
	sta (zp_tmp2),y
	iny
	inc zp_tmp9
	cpy zp_tmp6
	beq pravary1
	sty zp_tmp7
	jmp pravary0
pravary1
	lda zp_tmp2
	clc
	adc zp_tmp6
	sta zp_tmp2
	bcc pravarynoinc
	inc zp_tmp2+1
pravarynoinc
	jmp praloop
	

praendfr
	tya
	sec
	adc zp_tmp
	sta zp_tmp
	bcc pranoinc
	inc zp_tmp+1
pranoinc

praendanim

	rts

initreadyanimsprites
	lda #$00
	sta PRIOR
	sta ldprior+1

	lda #$48
	sta HPOSP0
	lda #$68
	sta HPOSP1
	lda #$88
	sta HPOSP2
	lda #$98
	sta HPOSP3
	lda #$ff
	sta HPOSM0
	sta HPOSM1
	sta HPOSM2
	sta HPOSM3

	lda #$00
	sta COLPM0
	sta COLPM1
	sta COLPM2
	sta COLPM3
	
	lda #$03
	sta SIZEP0
	sta SIZEP1
	sta SIZEP2
	sta SIZEP3
	lda #$ff
	sta SIZEM

	lda #$00
	sta GRAFP0
	sta GRAFP1
	sta GRAFP2
	sta GRAFP3
	sta GRAFM

	lda #$00
	sta GRACTL
	
	lda #$04
	sta PRIOR

	rts

	.if *>=hcm_dlist_end
	;address check based on advise from TeBe
	ert "Code before 'hcm_dlist_end' got too long. Relocate elsewhere... Address: "*
	.else
	.print "Space left before 'hcm_dlist_end': ",hcm_dlist_end-*," (address: ",*,")"
	.endif

	dlist_start = after_display
	hcm_dlist_end = after_display+$3f0

	org hcm_dlist_end

	.align $100
timerpattern
	.byte $1f, $20, $20, $1f, $20, $1f, $20
	
	timerpatternmax = 6
hcm_file_header_mixpmpf
	.sav [0] $10

hcm_file_header_dontmixpmpf
	.sav [$4020] $10

stripe_frame_delay_tmp
	.ds 28

readyanim_bridge_dlist
	.byte $70,$70,$70,$70,$70,$70,$70
	.rept 8
	.byte $48, <readyanim_bridge_screen, >readyanim_bridge_screen
	.byte $48, <(readyanim_bridge_screen+8), >(readyanim_bridge_screen+8)
	.endr
	.byte $41, <readyanim_bridge_dlist, >readyanim_bridge_dlist

readyanim_bridge_screen
	.byte %00001001
	.rept 6
	.byte %10011001
	.endr
	.byte %10010000
	.byte %00000110
	.rept 6
	.byte %01100110
	.endr
	.byte %01100000

ts_dlist
	.byte $70,$70,$f0
	.rept 8, #
	.byte $55, <(ts_screen+:1*48), >(ts_screen+:1*48)
	.endr

	.byte $d0
	.byte $10

	.rept 5, #
	.byte $54, <(ts_screen+(7-:1)*48), >(ts_screen+(7-:1)*48)
	.endr

	.byte $d4, <(ts_screen+2*48), >(ts_screen+2*48)
	.byte $d4, <(ts_screen+48), >(ts_screen+48)

	.byte $54, <ts_screen, >ts_screen

	.byte $41, <ts_dlist, >ts_dlist

ts_dlist2
	.byte $70,$70,$f0
	.rept 8, #
	.byte $55, <(ts_screen2+:1*48), >(ts_screen2+:1*48)
	.endr

	.byte $d0
	.byte $10

	.rept 5, #
	.byte $54, <(ts_screen2+(7-:1)*48), >(ts_screen2+(7-:1)*48)
	.endr

	.byte $d4, <(ts_screen2+2*48), >(ts_screen2+2*48)
	.byte $d4, <(ts_screen2+48), >(ts_screen2+48)

	.byte $54, <ts_screen2, >ts_screen2

	.byte $41, <ts_dlist2, >ts_dlist2

ts_screen_lines_lo
	.rept 10, #
	.byte <(ts_screen+:1*48)
	.endr

ts_screen_lines_hi
	.rept 10, #
	.byte >(ts_screen+:1*48)
	.endr

ts_screen2_lines_lo
	.rept 10, #
	.byte <(ts_screen2+:1*48)
	.endr

ts_screen2_lines_hi
	.rept 10, #
	.byte >(ts_screen2+:1*48)
	.endr

waitforvblank
	lda #$7c
waitforvcount
	cmp VCOUNT
	bne waitforvcount
	rts

recyclepic1data

rcy0	ldx #$00

	lda tsspritedata,x
	sta ts_sprites+$400,x
	sta ts_sprites+$600+4,x
	lda tsspritedata+$100,x
	sta ts_sprites+$500,x
	sta ts_sprites+$700+4,x

	lda #$00
	sta ts_sprites+$300,x
	sta ts_screen,x
	sta ts_screen2,x
	sta ts_screen+$100,x
	sta ts_screen2+$100,x
	inx
	bne rcy1

	lda #$60 ;rts
	sta rcy0
	rts

rcy1	stx rcy0+1
	rts
	

	ldx #$00
	lda #$00
shts0	sta ts_screen,x
	sta ts_screen2,x
	inx
	bne shts0

	ldx #$00
	lda #$00
shts1	sta ts_screen+$100,x
	sta ts_screen2+$100,x
	inx
	cpx #$e0
	bne shts1

	rts

showtsscreen
	lda #$00
	sta hcmena

	.ifndef debugtimers
	.ifndef testcolors
	lda #$00
	sta COLBK
	.endif
	.endif

	lda #<ts_dlist
	sta DLISTL
	lda #>ts_dlist
	sta DLISTH

	lda #>ts_char_data
	sta CHBASE

	lda #$23
	sta DMACTL

	lda #$0c
	sta HSCROL
	
	jsr opentsscreen	

	lda #$ff
	sta GRAFP0
	sta GRAFP1
	sta GRAFP2
	sta GRAFP3
	sta GRAFM

	lda #$00
	sta GRACTL
	lda #$01
	sta PRIOR
	
	lda #<tsanim_nmi
	sta $fffa
	lda #>tsanim_nmi
	sta $fffb

	lda #$80
	sta NMIEN

	rts

opentsscreen
	lda otspos
	sta HPOSP0
	lda otspos+1
	sta HPOSP1

	lda otspos+2
	sta HPOSM0
	lda otspos+3
	sta HPOSM1

	lda otspos+4
	sta HPOSM2
	lda otspos+5
	sta HPOSM3

	lda otspos+6
	sta HPOSP2
	lda otspos+7
	sta HPOSP3
	
	ldx #$03
ots0	lda otspos,x
	beq ots1
	dec otspos,x
ots1	lda otspos+4,x
	cmp #$ff
	beq ots2
	inc otspos+4,x
ots2	dex
	bpl ots0

ots3	lda otscol
	beq ots4
	sta COLPM0
	sta COLPM1
	sta COLPM2
	sta COLPM3
	inc ots3+1
	bne ots4
	inc ots3+2
ots4	
	rts

showtssprites

	lda #$00
	sta GRAFP0
	sta GRAFP1
	sta GRAFP2
	sta GRAFP3
	sta GRAFM

	lda #$23+4+8+$10
	sta DMACTL

	lda #$03
	sta GRACTL

	lda #$00
	sta SIZEP0
	sta SIZEP1
	sta SIZEP2
	sta SIZEP3
	sta SIZEM

	lda #>ts_sprites
	sta PMBASE

	lda mts0+1
	sta COLPM0
	sta COLPM1
	lda #$00
	sta COLPM2
	sta COLPM3

rollintssprites

	ldx #$03
rtss0	lda rtsspos,x
	sta HPOSP0,x
	dec rtsspos,x
	dex
	bpl rtss0

maintaintsscreen
mts0	ldy #$06
	tya
	ora tscolor
	sta COLPM0
	sta COLPM1
	lda mtscolormap,y
	sta mts0+1

	inc mts2+1
mts2	lda #$00
	and #$1f
	bne mts3
	
	lda tscolor
	clc
	adc #tscoloroffset
	ora #ts_pf0
	sta tspf0+1
	and #$f0
	ora #ts_pf1
	sta tspf1+1
	and #$f0
	ora #ts_pf2
	sta tspf2+1
	and #$f0
	ora #ts_pf3
	sta tspf3+1

	lda tscolor
	clc
	adc #$10
	sta tscolor

mts3	rts

tscolor	.byte 0

otspos	.byte $2c,$4c,$6c,$74
	.byte $84,$8c,$94,$b4

otscol	.byte 2,2,4,4,6,6,8,8,10,10,12,12,14,14,14,14,12,12,10,10,8,8,6,6,6,6,4,4,2,2,1
	.byte 0

rtsspos	.byte $bc+$1f,$c4+$1f,$be+$1f,$c6+$1f

mtscolormap
	.byte 6, 6, 6, 6, 6, 6, 6, 6, 6, 8, 8, 10, 10, 10, 10


tsanim_nmi
	sta nmisava+1
	
nmix	lda #$00
	beq dli1
	bmi dli3
	cmp #$02
	beq dlirefl

dli2
	lda timerena
	beq dli2skip

	sty nmisavy+1

	lda #$00
	sta IRQEN

	lda #$00
staaudf2
	sta AUDF1+(timer-1)*2

	ldy #$00
	lda (zpcode.sb2+1),y
staaudc
	sta sample_register

nmisavy	ldy #$00

dli2skip
	lda #$80
	jmp nmiend

	
dli3

	lda #$00
	sta zpcode.sb2+1
	inc zpcode.sb2+2

	lda timerena
	sta IRQEN

	jsr restarttimer

	lda #$00
	jmp nmiend

dlirefl

	lda #ts_refl_color
	sta WSYNC

	.ifndef debugtimers
	.ifndef testcolors
	sta COLBK
	.endif
	.endif

	lda #ts_refl_pf0
	sta COLPF0
	lda #ts_refl_pf1
	sta COLPF1
	lda #ts_refl_pf2
	sta COLPF2
	lda #ts_refl_pf3
	sta COLPF3

	lda #$04
	sta CHACTL

	lda #$01
	jmp nmiend

dli1
	
dots_sb_1st_lo
	lda #$00
	sta zpcode.sb2+1
dots_sb_1st_hi
	lda #$00
	sta zpcode.sb2+2

	lda timerena
	sta IRQEN
	beq dots_skrst

	jsr restarttimer
dots_skrst

	lda #$02

nmiend
	sta nmix+1
nmisava
	lda #$00
	rti

switchtsdl

	lda tsadl
	bne stsd_dl2

	lda #<ts_dlist
	sta DLISTL
	lda #>ts_dlist
	sta DLISTH

	rts

stsd_dl2
	lda #<ts_dlist2
	sta DLISTL
	lda #>ts_dlist2
	sta DLISTH

	rts
	
dotsscreen

	.ifndef debugtimers
	.ifdef testcolors
	lda #$12
	sta COLBK
	.endif
	.endif

	lda tsadl
	beq dots_dl2

	dec tsadl

	lda #<ts_screen_lines_lo
	sta dtsa_screen_lines_addr_lo+1
	lda #>ts_screen_lines_lo
	sta dtsa_screen_lines_addr_lo+2

	lda #<ts_screen_lines_hi
	sta dtsa_screen_lines_addr_hi+1
	lda #>ts_screen_lines_hi
	sta dtsa_screen_lines_addr_hi+2

	jmp dots_aftrdl

dots_dl2

	inc tsadl

	lda #<ts_screen2_lines_lo
	sta dtsa_screen_lines_addr_lo+1
	lda #>ts_screen2_lines_lo
	sta dtsa_screen_lines_addr_lo+2

	lda #<ts_screen2_lines_hi
	sta dtsa_screen_lines_addr_hi+1
	lda #>ts_screen2_lines_hi
	sta dtsa_screen_lines_addr_hi+2

dots_aftrdl

	jsr dotsanim

	.ifndef debugtimers
	.ifdef testcolors
	lda #$00
	sta COLBK
	.endif
	.endif

	jmp after_dotsscreen

dotsstep
dtss2	lda tssine_step_sine_lo
	sta dtss1+1
dtss4	lda tssine_step_sine_hi
	sta dtss3+1
		
	ldx #00
	stx dtss5+1
	ldy #$00
dtss0	lda tssine,y
	sta tspos,x

dtss5	lda #$00
	clc
dtss1	adc #$00
	sta dtss5+1
	tya
dtss3	adc #$00
	tay
	
	inx
	cpx #43
	bne dtss0
	
	inc dtss2+1
	lda dtss2+1
	cmp #$d0
	bne dtssd
	lda #$00
	sta dtss2+1
	sta dtss4+1
	jmp dtsse

dtssd	inc dtss4+1

dtsse	dec dtss0+1

	
	ldx #00

dtssc	ldy #$00
	sty dtss8+1

	lda tsscrollptr
	sta dtss6+1
	lda tsscrollptr+1
	sta dtss6+2

dtss6	ldy tsscrolltext
	bpl dtss9
	lda #<tsscrolltext
	sta dtss6+1
	lda #>tsscrolltext
	sta dtss6+2
	jmp dtss6

dtss9	lda tscharix_lo,y
	sta dtss7+1
	lda tscharix_hi,y
	sta dtss7+2

dtss8	ldy #00
dtss7	lda tsfont,y
	sta tschardata,x
	inx
	cpx #43
	beq dtssab
	iny
	cpy #$06
	bne dtss7

	ldy #$00
	sty dtss8+1

	inc dtss6+1
	bne dtss6
	inc dtss6+2
	jmp dtss6

dtssab	lda #$0c
	eor #$06
	sta dtssab+1
	sta HSCROL
	cmp #$0c
	bne dtssend

	inc dtssc+1
	lda dtssc+1
	cmp #$06
	bne dtssend

	lda #$00
	sta dtssc+1

	inc tsscrollptr
	bne dtssa
	inc tsscrollptr+1
dtssa	ldy #$00
	lda (tsscrollptr),y
	bpl dtssend

	lda #<tsscrolltext
	sta tsscrollptr
	lda #>tsscrolltext
	sta tsscrollptr+1

dtssend
	rts

dotsanim

	ldy #$07
dtsa2	

dtsa_screen_lines_addr_lo
	lda ts_screen_lines_lo,y
	sta dtsa_screen_addr+1
dtsa_screen_lines_addr_hi
	lda ts_screen_lines_hi,y
	sta dtsa_screen_addr+2

	ldx #43
dtsa1

	lda tschardata,x
	sta dtsa_data_addr+1

	lda tspos,x
	sta dtsa_data_addr+2

dtsa_data_addr
	lda ts_anim_data,y
dtsa_screen_addr
	sta ts_screen,x

	dex
	bpl dtsa1

	dey

	bpl dtsa2
	
	rts

	.align $100
tssine
	dta b(sin($13,$0f,$100))
	dta b(sin($13,$0f,$100))

tssine_step_sine_lo

	INS "tssine.dat"

	tssine_step_sine_hi = tssine_step_sine_lo+$100
	
tsfont
	INS "tsfont.dat"

tsspritedata
	INS "tssprites.dat"

tschardata
	.ds 43
tspos	.ds 43
tscharix_lo
	.rept $40, #
	.byte <(tsfont+:1*6)
	.endr
tscharix_hi
	.rept $40, #
	.byte >(tsfont+:1*6)
	.endr

smovesine
	INS "smovesine.dat"

start	sei

	.ifdef readyanim
	jsr doreadyanim
	jsr showreadyanim_bridgescreen
	.endif

	lda #$00
	sta NMIEN
	sta IRQEN

	lda #<zpcode.irq
	sta $fffe
	lda #>zpcode.irq
	sta $ffff
	
	ldx #<rmt_music_data	;low byte of RMT module to X reg
	ldy #>rmt_music_data	;hi byte of RMT module to Y reg
	lda #0			;starting song line 0-255 to A reg
	jsr rmt_player		;Init
	
	lda #0 ;stick with 64k timers 
	sta AUDCTL			
	lda #3
	sta SKCTL
	
	lda #<display
	sta dlist_scr_ptr
	lda #>display
	sta dlist_scr_ptr+1

	ldx #$c0
	lda #<dlist_start
	sta dlist_ptr+1
	lda #>dlist_start
	sta dlist_ptr+2
	lda #<dlist_start
	sta dljmplo+1
	lda #>dlist_start
	sta dljmphi+1
	jsr initdlist

	jsr copyzp
	jsr initsound
	jsr inittracker
	
	.ifndef debugtimers
	;pre-buffer one frame worth of sound
	;we use buff2 first as that's how the rest of the code starts out
	jsr dotracker
	jsr setsoundbuff2
	jsr dosound
	.endif
	
	jsr initrr
	jsr cleardisplay
	jsr genreadyanim_bridgehcm
	jsr inittsanim

	jsr waitforvblank

	lda #dma_setting
	sta DMACTL

	jsr initsprites
	jsr initfadein_pic1

	lda #<dlist_start
	sta DLISTL
	lda #>dlist_start
	sta DLISTH

	cli

loop	

	;swap sample buffers in raster code
	lda sb1+2
	eor #((samplebuf>>8)^(samplebuf2>>8))
	sta sb1+2
	sta sb3+2
	sta sb4+2
	sta dots_sb_1st_hi+1
	lda #<samplebuf
	sta sb1+1
	sta sb3+1
	sta sb4+1
	sta dots_sb_1st_lo+1

	bit hcmena
	bmi dohcm

	lda #$04
	jsr waitforvcount

	lda #$00
	sta CHACTL

tspf0	lda #ts_pf0+tscoloroffset
	sta COLPF0
tspf1	lda #ts_pf1+tscoloroffset
	sta COLPF1
tspf2	lda #ts_pf2+tscoloroffset
	sta COLPF2
tspf3	lda #ts_pf3+tscoloroffset
	sta COLPF3
	
	jmp dotsscreen
after_dotsscreen

	lda #$71
	jsr waitforvcount

	jsr handlesound

	.ifndef debugtimers
	.ifndef testcolors
	lda #$00
	sta COLBK
	.endif
	.endif

	jsr processstate
	
	jsr dotsstep
	jsr switchtsdl

	jmp loop
	
dohcm
	.ifndef debugtimers
	.ifdef testcolors
	lda #$04
	sta COLBK
	.endif
	.endif

	bit rrena
	bpl skiprr1
	jsr initdoemp
skiprr1

	.ifndef debugtimers
	.ifdef testcolors
	lda #$00
	sta COLBK
	.endif
	.endif

	lda #$0f
	jsr waitforvcount

	tsx
	stx spsave
	
	ldx #$00
	txs

	stx IRQEN

	sta WSYNC
	
stxaudf
	stx AUDF1+(timer-1)*2
	stx zpcode.irqsplcnt+1

lineloop
sb1	ldy samplebuf

	.rept 8, #
	.if :1==0
	;can spend 0 cycles in here
	.else
	;can spend 11 cycles in here
doempfrom:1
	cpx #$ff
	bne stillgoes:1

	jmp doemptylines

	.endif
	
stillgoes:1
	sta WSYNC

	lda #$48 ;hpos
sthpos1_1_:1
	sta HPOSP2
	sta HPOSP0


	.if :1==0
styaudc
	sty sample_register
	nop
	.elseif :1==1
	inc sb1+1	
	.else
	nop
	nop
	nop
	.endif

	ldy sprite_replica_start+$20, x
	lda sprite_replica_start+$120, x
	tax

	lda #$98
	sta HPOSP0
	sty GRAFP0
sthpos1_2_:1
	sta HPOSP2
stgraf1_:1
	stx GRAFP2
	
	tsx
	inx
	txs
	.endr

	cpx #$c0
	beq afterhcm

	jmp lineloop

afterhcm	
	ldx spsave
	txs

	jsr swapirqsamplebuffers

	jsr restarttimer
	lda timerena
	beq noirq

	brk
	nop ;brk / rti padding

afterbrk
	jsr handlesound
	
	bit rrena
	bpl skiprr0
	
	bit rrsena
	bpl skiprrs
	
	jsr stepscroller
	
skiprrs

	jsr cleanuprr

skiprr0
	jsr processstate
	jmp loop

noirq
	sta IRQEN
	jmp afterbrk

swapirqsamplebuffers
	lda zpcode.sb2+2
	eor #(((samplebuf>>8)+1)^((samplebuf2>>8)+1))
	sta zpcode.sb2+2
	lda #$00
	sta zpcode.sb2+1
	rts

handlesound
	.ifndef debugtimers
	.ifdef testcolors
	lda #$86
	sta COLBK
	.endif
	.endif

	.ifndef debugtimers
	jsr dotracker
	.endif
	
	.ifndef debugtimers
	.ifdef testcolors
	lda #$26
	sta COLBK
	.endif
	.endif

	.ifndef debugtimers
	jsr dosound
	.endif
	
	.ifndef debugtimers
	.ifdef testcolors
	lda #$fc
	sta COLBK
	.endif
	.endif
		
	.ifdef rmt
	jsr rmt_player+3		;play
	.endif

	.ifndef debugtimers
	.ifdef testcolors
	lda #$00
	sta COLBK
	.endif
	.endif
	rts
	
processstate
	.ifndef debugtimers
	.ifdef testcolors
	lda #$ac
	sta COLBK
	.endif
	.endif

	bit HARDBASS_SOUND_DISABLE
	bpl ps2
	lda #$00
	sta ps1+1
	jmp ps0	

ps2	inc ps1+1
ps1	lda #$00
	cmp #$02
	bne ps0
	
	lda instrFlags
	bpl ps0
	
	;scroller/etc... color flash effect
	lda #$0e
	sta doempbgcol+1
	sta mts0+1

ps0
	jsr dostatemachine

	.ifndef debugtimers
	.ifdef testcolors
	lda #$00
	sta COLBK
	.endif
	.endif
	rts	
	
hcmena	.byte $ff
rrena	.byte $00
rrrena	.byte $ff
rrsena	.byte $00
tsadl	.byte $00

restarttimer
	lda #timerpatternmax-1
	sta zpcode.irqtpt+1

	lda timerpattern+timerpatternmax
	sta WSYNC

staaudf	
	sta AUDF1+(timer-1)*2

	rts

initdoemp

	ldy #$00
rdoffs	lda (zp_tmp),y
	cmp #$ff
	bne norrend
	
	lda #<rrdata
	sta zp_tmp
	lda #>rrdata
	sta zp_tmp+1
	jmp rdoffs
	
norrend	cmp #$fe
	bne nopd
	
	lda #$00
	sta zp_tmp
	inc zp_tmp+1
	jmp rdoffs

nopd
	clc
masteroffs
	adc #$a0
	sta idoffs+1
	and #$07
	bne idoffsok
	inc idoffs+1
	lda idoffs+1
	and #$07

idoffsok
	lsr
	sta idlgstart+1

	iny
	lda (zp_tmp),y
	sta dlnumlines+1
	sta dlframelen+1
	clc
	adc idoffs+1
	sta idtil+1

	lda zp_tmp+1
	sta rraddr1+2
	sta rraddr3+2
	sta rraddr5+2
	sta rraddr7+2

	iny
	lda (zp_tmp),y
	sta idadd+1

	iny
	lda (zp_tmp),y
	clc
	adc #<rrdldata
	sta dlframe+1

	iny
	lda (zp_tmp),y
	adc #>rrdldata
	sta dlframe+2

	lda zp_tmp
	clc
	adc #$05
idlgstart
	ldy #$00
	ldx #$00
idcalloc
	sta idstartb+1
	lda rraddr_lo,y
	sta idrraddr+1
	lda rraddr_hi,y
	sta idrraddr+2
idstartb
	lda #$00
idrraddr
	sta rraddr1+1
	
	iny
	cpy #$04
	bne idaddit
	ldy #$00
	
idaddit

	clc
idadd	adc #$00
	bcc idinx

	bit rrrena
	bpl idskipr0

	inc zp_tmp+1
	
idskipr0

idinx	inx
	cpx #$04
	bne idcalloc

	bit rrrena
	bpl idskipr1

	sta zp_tmp

idskipr1

	lda rraddr1+1

idoffs
	ldy #$02
idtil
	ldx #$12
	
	jsr genrr

	tya
	sta doempfrom1+1
	sta doempfrom2+1
	sta doempfrom3+1
	sta doempfrom4+1
	sta doempfrom5+1
	sta doempfrom6+1
	sta doempfrom7+1
	sta origfrom+1

	and #$07
	tay
	lda doemp_jmp_lo,y
	sta doempstartjmp +1
	lda doemp_jmp_hi,y
	sta doempstartjmp +2

origfrom
	lda #$00
	lsr
	lsr
	lsr
	sec
	adc sb1+1
	sta sb3+1

	inx
	stx doempuntil0+1
	stx doempuntil1+1
	stx doempuntil2+1
	stx doempuntil3+1
	stx doempuntil4+1
	stx doempuntil5+1
	stx doempuntil6+1
	stx doempuntil7+1

	txa
	and #$07
	tay
	lda stillgoes_jmp_lo,y
	sta doempjmp+1
	lda stillgoes_jmp_hi,y
	sta doempjmp+2

	txa
	lsr
	lsr
	lsr
	clc
	adc sb4+1
	sta sb4+1
sb4	lda samplebuf
	sta doempendspl2+1

	txa
	ldx sb4+1

	and #$06
	beq storedoempendspl

	inx

storedoempendspl
	stx doempendspl1+1

	rts


doemptylines

	lda #$04
	sta PRIOR

doempbgcol
	ldy #$0e
	sty COLPF1
	lda doempbgmap,y
	sta doempbgcol+1
	lda #$00
	sta COLPF2

doempstartjmp
	jmp doemp0

doemploop

	.rept 8, #

doemp:1
	.if :1==0
sb3	ldy samplebuf
styaudc2
	sty sample_register
	inc sb3+1
	.elseif (:1==1)|(:1==3)|(:1==5)|(:1==7)
rraddr:1
	lda rrdata
	sta COLPF2
	inc rraddr:1+1
	.else
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	.endif


	inx

doempuntil:1
	cpx #$81
	beq doempend

	sta WSYNC

	.endr

	jmp doemploop

doempend
	txs

doempendspl1
	lda #$00
	sta sb1+1

ldprior
	ldx #$00
empcr1	lda #$04
empcr2	ldy #$06
	sta COLPF1
	sty COLPF2
	stx PRIOR
	tsx

doempendspl2
	ldy #$00

doempjmp
	jmp stillgoes0

genrr
	stx gmdx+1
	sty gmdy+1

	stx clrrx+1
	sty clrry+1

	lda sprite_start+$31f,y
	sta restpm0+1
	lda sprite_start+$41f,y
	sta restpm1+1
	lda sprite_start+$51f,y
	sta restpm2+1
	lda sprite_start+$61f,y
	sta restpm3+1
	lda sprite_start+$71f,y
	sta restpm4+1
	lda sprite_replica_start+$1f,y
	sta restpm5+1
	lda sprite_replica_start+$11f,y
	sta restpm6+1
	lda sprite_start+$320,x
	sta restpm7+1
	lda sprite_start+$420,x
	sta restpm8+1
	lda sprite_start+$520,x
	sta restpm9+1
	lda sprite_start+$620,x
	sta restpma+1
	lda sprite_start+$720,x
	sta restpmb+1
	lda sprite_replica_start+$20,x
	sta restpmc+1
	lda sprite_replica_start+$120,x
	sta restpmd+1

	lda #$00
	sta sprite_start+$31f,y
	sta sprite_start+$41f,y
	sta sprite_start+$51f,y
	sta sprite_start+$61f,y
	sta sprite_start+$71f,y
	sta sprite_replica_start+$1f,y
	sta sprite_replica_start+$11f,y
	sta sprite_start+$320,x
	sta sprite_start+$420,x
	sta sprite_start+$520,x
	sta sprite_start+$620,x
	sta sprite_start+$720,x
	sta sprite_replica_start+$20,x
	sta sprite_replica_start+$120,x

	lda dlistjmptbl_lo,y
	sta dlist_ptr+1
	sta restdlptrlo+1
	lda dlistjmptbl_hi,y
	sta dlist_ptr+2
	sta restdlptrhi+1

	lda #$01
	jsr store_dl_byte

	jsr read_dl_byte
	sta restdlo+1

dlnumlines
	ldy #$00

	lda middle_dlistjmptbl_lo,y
	sta dlmemaddrlo+1
	sta dlmemaddrhi+1
	jsr store_dl_byte

	jsr read_dl_byte
	sta restdhi+1

	lda middle_dlistjmptbl_hi,y
	sta dlmemaddrlo+2
	sta dlmemaddrhi+2
	jsr store_dl_byte

	inx
	inx
	lda dlistjmptbl_lo,x
	sta middle_dlist_jmp_addr
	lda dlistjmptbl_hi,x
	sta middle_dlist_jmp_addr+1

	ldx #$01
dlframe
	ldy rrdldata

	lda middle_display_data_lo,y
	clc
dlmemaddroffs
	adc #$00
dlmemaddrlo
	sta middle_dlist,x
	inx
	lda middle_display_data_hi,y
	adc #$00
dlmemaddrhi
	sta middle_dlist,x
	inx
	inx

	inc dlframe+1
	bne dlnoinc
	inc dlframe+2

dlnoinc

dlframelen
	lda #$00
	dec dlframelen+1
	bne dlframe

gmdx	ldx #$00
gmdy	ldy #$00
	rts

showrrtop
	lda #$00
	sta masteroffs+1
	jmp showrr

showrrbottom
	lda #$a4
	sta masteroffs+1

showrr	lda #$ff
	sta rrena
	
	lda #<(rrdata+$300)
	sta zp_tmp
	lda #>(rrdata+$300)
	sta zp_tmp+1

	lda #$00
	sta ssrepspc+1
	
	rts

moverrpersine
mrr0	ldx #$00
	lda smovesine,x
	sta masteroffs+1
	inx
	cpx #$60
	bne mrr1
	ldx #$00
	
mrr1	stx mrr0+1
	
	rts

hiderr	lda #$00
	sta rrena
	rts

stoprrr	lda #$00
	sta rrrena
	rts

startrrr
	lda #$ff
	sta rrrena
	rts

startrrs
	lda #$ff
	sta rrsena
	rts

cleanuprr

	sty clrryb+1
clrry
	ldy #$00

restpm0	lda #$00
	sta sprite_start+$31f,y
restpm1	lda #$00
	sta sprite_start+$41f,y
restpm2	lda #$00
	sta sprite_start+$51f,y
restpm3	lda #$00
	sta sprite_start+$61f,y
restpm4	lda #$00
	sta sprite_start+$71f,y
restpm5	lda #$00
	sta sprite_replica_start+$1f,y
restpm6	lda #$00
	sta sprite_replica_start+$11f,y

clrrx
	ldy #$00
restpm7	lda #$00
	sta sprite_start+$320,y
restpm8	lda #$00
	sta sprite_start+$420,y
restpm9	lda #$00
	sta sprite_start+$520,y
restpma	lda #$00
	sta sprite_start+$620,y
restpmb	lda #$00
	sta sprite_start+$720,y
restpmc	lda #$00
	sta sprite_replica_start+$20,y
restpmd	lda #$00
	sta sprite_replica_start+$120,y

restdlptrlo
	lda #$00
	sta dlist_ptr+1
restdlptrhi
	lda #$00
	sta dlist_ptr+2

	lda #$4e
	jsr store_dl_byte

restdlo
	lda #$00
	jsr store_dl_byte
restdhi
	lda #$00
	jsr store_dl_byte

	lda #$ff
	sta doempfrom1+1
	sta doempfrom2+1
	sta doempfrom3+1
	sta doempfrom4+1
	sta doempfrom5+1
	sta doempfrom6+1
	sta doempfrom7+1

clrryb	ldy #$00
	rts

ffwscroller
	lda #$0c
	sta hscrolv+1
	rts

stepscroller
	dec hscrolv+1
hscrolv
	lda #$0f
	cmp #$0b
	bne ssnostepyet

ssbuffoffs
	ldx #$00

sscol	lda #$00
	eor #$01
	sta sscol+1
	beq ssdotextstep

readsstext

ssrepspc
	lda #$00
	beq ssaftrrepspc
	dec ssrepspc+1
	ldy #$00
	jmp ssaftr

ssaftrrepspc

sstext
	ldy scrolltext
	bpl ssaftr
	cpy #$ff
	beq ssreset

	tya
	and #$7f
	sta ssrepspc+1
	jmp readsstext

ssreset
	lda #<scrolltext
	sta sstext+1
	lda #>scrolltext
	sta sstext+2
	jmp readsstext

ssdotextstep
	lda ssrepspc+1
	bne readsstext

	inc sstext+1
	bne sstext
	inc sstext+2
	jmp readsstext

ssaftr

	inx
	cpx #$21
	bne ssnobuffoffsreset

	ldx #$00

ssnobuffoffsreset
	stx ssbuffoffs+1
	stx dlmemaddroffs+1

	lda sscol+1
	bne ssdocol2

	cpx #$00
	beq sscol1sngl

	jsr writescr_col1_dbl
	jmp ssaftr2

sscol1sngl
	jsr writescr_col1
	jmp ssaftr2

ssdocol2
	cpx #$00
	beq sscol2sngl

	jsr writescr_col2_dbl
	jmp ssaftr2

sscol2sngl
	jsr writescr_col2

ssaftr2

	lda #$0f
	sta hscrolv+1

ssnostepyet
	sta HSCROL
	rts

writescr_col1
	lda scrfont_col1,y
	sta middle_display_mem+$20,x
	lda scrfont_col1+$40,y
	sta middle_display_mem+$20+middle_display_data_line_bytes,x
	lda scrfont_col1+$40*2,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*2,x
	lda scrfont_col1+$40*3,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*3,x
	lda scrfont_col1+$40*4,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*4,x
	lda scrfont_col1+$40*5,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*5,x
	lda scrfont_col1+$40*6,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*6,x
	lda scrfont_col1+$40*7,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*7,x
	lda scrfont_col1+$40*8,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*8,x
	lda scrfont_col1+$40*9,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*9,x
	lda scrfont_col1+$40*10,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*10,x
	lda scrfont_col1+$40*11,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*11,x
	lda scrfont_col1+$40*12,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*12,x
	lda scrfont_col1+$40*13,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*13,x
	lda scrfont_col1+$40*14,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*14,x
	lda scrfont_col1+$40*15,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*15,x
	rts
	
writescr_col1_dbl
	lda scrfont_col1,y
	sta middle_display_mem+$20,x
	sta middle_display_mem-1,x
	lda scrfont_col1+$40,y
	sta middle_display_mem+$20+middle_display_data_line_bytes,x
	sta middle_display_mem-1+middle_display_data_line_bytes,x
	lda scrfont_col1+$40*2,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*2,x
	sta middle_display_mem-1+middle_display_data_line_bytes*2,x
	lda scrfont_col1+$40*3,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*3,x
	sta middle_display_mem-1+middle_display_data_line_bytes*3,x
	lda scrfont_col1+$40*4,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*4,x
	sta middle_display_mem-1+middle_display_data_line_bytes*4,x
	lda scrfont_col1+$40*5,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*5,x
	sta middle_display_mem-1+middle_display_data_line_bytes*5,x
	lda scrfont_col1+$40*6,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*6,x
	sta middle_display_mem-1+middle_display_data_line_bytes*6,x
	lda scrfont_col1+$40*7,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*7,x
	sta middle_display_mem-1+middle_display_data_line_bytes*7,x
	lda scrfont_col1+$40*8,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*8,x
	sta middle_display_mem-1+middle_display_data_line_bytes*8,x
	lda scrfont_col1+$40*9,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*9,x
	sta middle_display_mem-1+middle_display_data_line_bytes*9,x
	lda scrfont_col1+$40*10,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*10,x
	sta middle_display_mem-1+middle_display_data_line_bytes*10,x
	lda scrfont_col1+$40*11,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*11,x
	sta middle_display_mem-1+middle_display_data_line_bytes*11,x
	lda scrfont_col1+$40*12,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*12,x
	sta middle_display_mem-1+middle_display_data_line_bytes*12,x
	lda scrfont_col1+$40*13,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*13,x
	sta middle_display_mem-1+middle_display_data_line_bytes*13,x
	lda scrfont_col1+$40*14,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*14,x
	sta middle_display_mem-1+middle_display_data_line_bytes*14,x
	lda scrfont_col1+$40*15,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*15,x
	sta middle_display_mem-1+middle_display_data_line_bytes*15,x
	rts

writescr_col2
	lda scrfont_col2,y
	sta middle_display_mem+$20,x
	lda scrfont_col2+$40,y
	sta middle_display_mem+$20+middle_display_data_line_bytes,x
	lda scrfont_col2+$40*2,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*2,x
	lda scrfont_col2+$40*3,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*3,x
	lda scrfont_col2+$40*4,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*4,x
	lda scrfont_col2+$40*5,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*5,x
	lda scrfont_col2+$40*6,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*6,x
	lda scrfont_col2+$40*7,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*7,x
	lda scrfont_col2+$40*8,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*8,x
	lda scrfont_col2+$40*9,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*9,x
	lda scrfont_col2+$40*10,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*10,x
	lda scrfont_col2+$40*11,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*11,x
	lda scrfont_col2+$40*12,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*12,x
	lda scrfont_col2+$40*13,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*13,x
	lda scrfont_col2+$40*14,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*14,x
	lda scrfont_col2+$40*15,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*15,x
	rts

writescr_col2_dbl
	lda scrfont_col2,y
	sta middle_display_mem+$20,x
	sta middle_display_mem-1,x
	lda scrfont_col2+$40,y
	sta middle_display_mem+$20+middle_display_data_line_bytes,x
	sta middle_display_mem-1+middle_display_data_line_bytes,x
	lda scrfont_col2+$40*2,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*2,x
	sta middle_display_mem-1+middle_display_data_line_bytes*2,x
	lda scrfont_col2+$40*3,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*3,x
	sta middle_display_mem-1+middle_display_data_line_bytes*3,x
	lda scrfont_col2+$40*4,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*4,x
	sta middle_display_mem-1+middle_display_data_line_bytes*4,x
	lda scrfont_col2+$40*5,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*5,x
	sta middle_display_mem-1+middle_display_data_line_bytes*5,x
	lda scrfont_col2+$40*6,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*6,x
	sta middle_display_mem-1+middle_display_data_line_bytes*6,x
	lda scrfont_col2+$40*7,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*7,x
	sta middle_display_mem-1+middle_display_data_line_bytes*7,x
	lda scrfont_col2+$40*8,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*8,x
	sta middle_display_mem-1+middle_display_data_line_bytes*8,x
	lda scrfont_col2+$40*9,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*9,x
	sta middle_display_mem-1+middle_display_data_line_bytes*9,x
	lda scrfont_col2+$40*10,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*10,x
	sta middle_display_mem-1+middle_display_data_line_bytes*10,x
	lda scrfont_col2+$40*11,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*11,x
	sta middle_display_mem-1+middle_display_data_line_bytes*11,x
	lda scrfont_col2+$40*12,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*12,x
	sta middle_display_mem-1+middle_display_data_line_bytes*12,x
	lda scrfont_col2+$40*13,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*13,x
	sta middle_display_mem-1+middle_display_data_line_bytes*13,x
	lda scrfont_col2+$40*14,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*14,x
	sta middle_display_mem-1+middle_display_data_line_bytes*14,x
	lda scrfont_col2+$40*15,y
	sta middle_display_mem+$20+middle_display_data_line_bytes*15,x
	sta middle_display_mem-1+middle_display_data_line_bytes*15,x
	rts

initrr
	;init dl jump addrs
	ldy #$00
idlj0	lda #<dlist_start
	sta dlistjmptbl_lo,y
idlj1	lda #>dlist_start
	sta dlistjmptbl_hi,y

	lda idlj0+1
	clc
	adc #$03
	sta idlj0+1
	bcc idlj2
	inc idlj1+1
idlj2
	iny
	cpy #$c0
	bne idlj0
	
	;init rr dl jump addrs (reverse order: n-1=>1)
	ldy #$00
imdlj0	lda #<(middle_dlist+3*middle_dlist_max_lines)
	sta middle_dlistjmptbl_lo,y
imdlj1	lda #>(middle_dlist+3*middle_dlist_max_lines)
	sta middle_dlistjmptbl_hi,y

	lda imdlj0+1
	sec
	sbc #$03
	sta imdlj0+1
	bcs imdlj2
	dec imdlj1+1
imdlj2
	iny
	cpy #middle_dlist_max_lines
	bne imdlj0

	;init rr dl address byte addrs
	ldy #$00
imdl0	lda #<(middle_dlist+1)
	sta middle_dlisttbl_lo_lo,y
imdl1	lda #>(middle_dlist+1)
	sta middle_dlisttbl_lo_hi,y

	lda imdl0+1
	clc
	adc #$03
	sta imdl0+1
	bcs imdl2
	dec imdl1+1
imdl2
	iny
	cpy #middle_dlist_max_lines
	bne imdl0

	;init middle display mem addrs
	ldy #$00
imddlj0	lda #<middle_display_mem
	sta middle_display_data_lo,y
imddlj1	lda #> middle_display_mem
	sta middle_display_data_hi,y

	lda imddlj0+1
	clc
	adc #middle_display_data_line_bytes
	sta imddlj0+1
	bcc imddlj2
	inc imddlj1+1
imddlj2
	iny
	cpy #middle_display_data_lines
	bne imddlj0

	;init rr data ptr
	lda #<rrdata
	sta zp_tmp
	lda #>rrdata
	sta zp_tmp+1

	ldx #$00
icll	lda middle_display_data_lo,x
	sta iclmm+1
	lda middle_display_data_hi,x
	sta iclmm+2
	lda #$55
	ldy #$00
iclmm	sta middle_display_mem,y
	iny
	cpy #middle_display_data_line_bytes
	bne iclmm
	inx
	cpx #middle_display_data_lines
	bne icll

	ldx #$00
	ldy scrolltext
	jsr writescr_col1
	lda #$0f
	sta HSCROL

	rts

initdlist
	;header
	lda #$70
	jsr store_dl_byte
	jsr store_dl_byte
	jsr store_dl_byte

dlloop	lda #$4e
	jsr store_dl_byte
	lda dlist_scr_ptr
	jsr store_dl_byte
	clc
	adc #$20
	sta dlist_scr_ptr
	lda dlist_scr_ptr+1
	jsr store_dl_byte
	adc #$00
	sta dlist_scr_ptr+1
	dex
	bne dlloop
	
	;footer
	lda #$41
	jsr store_dl_byte
dljmplo	lda #$00
	jsr store_dl_byte
dljmphi	lda #$00
	jsr store_dl_byte
		
	rts
	
store_dl_byte
dlist_ptr
	sta dlist_start
	inc dlist_ptr+1
	bne str_dle
	inc dlist_ptr+2
str_dle	rts

read_dl_byte
	lda dlist_ptr+1
	sta rdbptr+1
	lda dlist_ptr+2
	sta rdbptr+2
rdbptr	lda $ffff
	rts

dlist_scr_ptr
	.byte <display,>display

inittsanim

	ldx #$00
itsa0	lda tssine,x
	and #$0f
	clc
	adc #>ts_anim_data
	sta tssine,x
	sta tssine+$100,x
	inx
	bne itsa0

	;let's sneak in PF3 into a frame or two
	ldx #$00
itsa2	lda ts_anim_data,x
	ora #$80
	sta ts_anim_data,x
	lda ts_anim_data+$100,x
	ora #$80
	sta ts_anim_data+$100,x
	lda ts_anim_data+$f00,x
	ora #$80
	sta ts_anim_data+$f00,x
	inx
	bne itsa2
	
	lda #<tsscrolltext
	sta tsscrollptr
	lda #>tsscrolltext
	sta tsscrollptr+1

	rts

initblackstripes
initstripes
	lda #$20
	sta zp_tmp3
	sta zp_tmp5
	lda #$00
	sta zp_tmp8

	ldx #$00
initstripes0
	lda #$ff
	sta stripe_char_pos,x
	lda stripe_char_speed,x
	sta stripe_char_speed_cnt,x
	lda stripe_frame_delay,x
	sta stripe_frame_delay_tmp,x
	inx
	cpx #28
	bne initstripes0
	rts

dostripes
	ldx #27
donextstripe

	ldy stripe_char_pos,x
	cpy #23
	beq nexstripe

	lda stripe_frame_delay_tmp,x
	beq nostripedelay

	dec stripe_frame_delay_tmp,x
	jmp nexstripe

nostripedelay

	dec stripe_char_speed_cnt,x
	bne nexstripe	

	lda stripe_char_speed,x
	sta stripe_char_speed_cnt,x

	iny
	tya
	sta stripe_char_pos,x
	
	jmp putimgchar
after_putimgchar

nexstripe

	dex
	bpl donextstripe
	rts

initfadein_pic1
	lda #>(pic1data+hcm_file_pic_data_start_offset)
	sta copyimg_img_addr_hi+1

	lda #<copyul_pic1_ul1_hi
	sta copyimg_ul1_addrs_hi+1
	lda #>copyul_pic1_ul1_hi
	sta copyimg_ul1_addrs_hi+2

	lda #<copyul_pic1_ul2_hi
	sta copyimg_ul2_addrs_hi+1
	lda #>copyul_pic1_ul2_hi
	sta copyimg_ul2_addrs_hi+2

	lda #$60
	sta ifdin0
	lda #$04
	sta dfiminy+1
	lda #20
	sta dfimaxy+1

	jmp initfadein_mixpmpf

initfadein_pic2
	lda #>(pic2data+hcm_file_pic_data_start_offset)
	sta copyimg_img_addr_hi+1

	lda #<copyul_pic2_ul1_hi
	sta copyimg_ul1_addrs_hi+1
	lda #>copyul_pic2_ul1_hi
	sta copyimg_ul1_addrs_hi+2

	lda #<copyul_pic2_ul2_hi
	sta copyimg_ul2_addrs_hi+1
	lda #>copyul_pic2_ul2_hi
	sta copyimg_ul2_addrs_hi+2

	lda #$4c
	sta ifdin0
	lda #$00
	sta dfiminy+1
	lda #24
	sta dfimaxy+1

	jmp initfadein_mixpmpf

initfadein_pic3
	lda #>(pic3data+hcm_file_pic_data_start_offset)
	sta copyimg_img_addr_hi+1

	lda #<copyul_pic3_ul1_hi
	sta copyimg_ul1_addrs_hi+1
	lda #>copyul_pic3_ul1_hi
	sta copyimg_ul1_addrs_hi+2

	lda #<copyul_pic3_ul2_hi
	sta copyimg_ul2_addrs_hi+1
	lda #>copyul_pic3_ul2_hi
	sta copyimg_ul2_addrs_hi+2

	lda #$60
	sta ifdin0
	lda #$00
	sta dfiminy+1
	lda #24
	sta dfimaxy+1

	jmp initfadein_dontmixpmpf

initfadein_mixpmpf
	lda #$00
	sta PRIOR
	sta ldprior+1

	lda #$48
	sta HPOSP0
	sta HPOSP2
	lda #$68
	sta HPOSP1
	sta HPOSP3
	lda #$88
	sta HPOSM0
	sta HPOSM2
	lda #$90
	sta HPOSM1
	sta HPOSM3

	lda #<HPOSP2
	.rept 8, #
	sta sthpos1_1_:1+1
	sta sthpos1_2_:1+1
	.endr
	lda #<GRAFP2
	.rept 8, #
	sta stgraf1_:1+1
	.endr

	lda hcm_file_header_mixpmpf+hcm_file_palette_colors_offset+color_bck
	sta COLBK
	
	lda hcm_file_header_mixpmpf+hcm_file_palette_colors_offset+color_underlay1
	sta COLPM0
	sta COLPM1
	
	lda hcm_file_header_mixpmpf+hcm_file_palette_colors_offset+color_underlay2
	sta COLPM2
	sta COLPM3
	
	lda hcm_file_header_mixpmpf+hcm_file_palette_colors_offset+color_playfield0
	sta COLPF0
	lda hcm_file_header_mixpmpf+hcm_file_palette_colors_offset+color_playfield1
	sta COLPF1
	sta empcr1+1
	lda hcm_file_header_mixpmpf+hcm_file_palette_colors_offset+color_playfield2
	sta COLPF2
	sta empcr2+1

	lda #<copyul_screen_ul1_hi
	sta copyimg_screen_ul1_addrs_hi+1
	sta putimg_screen_ul1_addrs_hi+1
	lda #>copyul_screen_ul1_hi
	sta copyimg_screen_ul1_addrs_hi+2
	sta putimg_screen_ul1_addrs_hi+2

	lda #<copyul_screen_ul2_hi
	sta copyimg_screen_ul2_addrs_hi+1
	sta putimg_screen_ul2_addrs_hi+1
	lda #>copyul_screen_ul2_hi
	sta copyimg_screen_ul2_addrs_hi+2
	sta putimg_screen_ul2_addrs_hi+2

	lda #<copyul_mask
	sta copyimg_mask_addr+1
	sta putimg_mask_addr+1
	lda #>copyul_mask
	sta copyimg_mask_addr+2
	sta putimg_mask_addr+2

	jmp initfadein

initfadein_dontmixpmpf
	lda #$24
	sta PRIOR
	sta ldprior+1

	lda #$48
	sta HPOSP0
	sta HPOSP1
	lda #$68
	sta HPOSP2
	sta HPOSP3
	lda #$88
	sta HPOSM0
	sta HPOSM1
	lda #$90
	sta HPOSM2
	sta HPOSM3

	lda #<HPOSP1
	.rept 8, #
	sta sthpos1_1_:1+1
	sta sthpos1_2_:1+1
	.endr
	lda #<GRAFP1
	.rept 8, #
	sta stgraf1_:1+1
	.endr

	lda hcm_file_header_dontmixpmpf+hcm_file_palette_colors_offset+color_bck
	sta COLBK
	
	lda hcm_file_header_dontmixpmpf+hcm_file_palette_colors_offset+color_underlay1
	ora #pic3color
	sta COLPM0
	sta COLPM2
	
	lda hcm_file_header_dontmixpmpf+hcm_file_palette_colors_offset+color_underlay2
	ora #pic3color
	sta COLPM1
	sta COLPM3
	
	lda hcm_file_header_dontmixpmpf+hcm_file_palette_colors_offset+color_playfield0
	ora #pic3color
	sta COLPF0
	lda hcm_file_header_dontmixpmpf+hcm_file_palette_colors_offset+color_playfield1
	ora #pic3color
	sta COLPF1
	sta empcr1+1
	lda hcm_file_header_dontmixpmpf+hcm_file_palette_colors_offset+color_playfield2
	ora #pic3color
	sta COLPF2
	sta empcr2+1

	lda #<copyul_screen_nonmix_ul1_hi
	sta copyimg_screen_ul1_addrs_hi+1
	sta putimg_screen_ul1_addrs_hi+1
	lda #>copyul_screen_nonmix_ul1_hi
	sta copyimg_screen_ul1_addrs_hi+2
	sta putimg_screen_ul1_addrs_hi+2

	lda #<copyul_screen_nonmix_ul2_hi
	sta copyimg_screen_ul2_addrs_hi+1
	sta putimg_screen_ul2_addrs_hi+1
	lda #>copyul_screen_nonmix_ul2_hi
	sta copyimg_screen_ul2_addrs_hi+2
	sta putimg_screen_ul2_addrs_hi+2

	lda #<copyul_nonmix_mask
	sta copyimg_mask_addr+1
	sta putimg_mask_addr+1
	lda #>copyul_nonmix_mask
	sta copyimg_mask_addr+2
	sta putimg_mask_addr+2

	jmp initfadein

initfadein
	lda #$ff
	sta hcmena

	lda #$20
	sta zp_tmp2
	sta zp_tmp3
	sta zp_tmp4
	sta zp_tmp5
	ldx #$00
	stx zp_tmp7
	stx zp_tmp8
	ldy #$00
ifdin0	jmp copyimgchar

pic3copychar00
	ldy #$00
	jmp copyimgchar

dofadeinquick
	jsr dofadein
	jsr dofadein
	jsr dofadein
	jmp dofadein

dofadein
	;xorshift a

	lda xorshift_state
	sta xorshift_state_tmp
	lda xorshift_state+1
	sta xorshift_state_tmp+1
	
	asl xorshift_state
	lda xorshift_state+1
	rol
	and #$03
	eor xorshift_state_tmp+1
	sta xorshift_state+1
	sta xorshift_state_tmp+1
	
	lda xorshift_state
	eor xorshift_state_tmp
	sta xorshift_state
	sta xorshift_state_tmp
	
	;xorshift b

	lsr xorshift_state+1
	lda xorshift_state
	ror
	eor xorshift_state_tmp
	sta xorshift_state
	sta xorshift_state_tmp
	
	lda xorshift_state+1
	eor xorshift_state_tmp+1
	sta xorshift_state+1
	sta xorshift_state_tmp+1
	
	;xorshift c

	asl xorshift_state
	rol xorshift_state+1
	asl xorshift_state
	rol xorshift_state+1
	asl xorshift_state
	lda xorshift_state+1
	rol
	and #$03
	eor xorshift_state_tmp+1
	sta xorshift_state+1
	sta xorshift_state_tmp+1
	
	lda xorshift_state
	eor xorshift_state_tmp
	sta xorshift_state
	sta xorshift_state_tmp

	;end of xorshift

	lda xorshift_state
	and #$1f
	cmp #28
	bpl dfiskipchar

	tax
	stx zp_tmp7
	stx zp_tmp8

	lsr xorshift_state_tmp+1
	ror xorshift_state_tmp
	lsr xorshift_state_tmp+1
	ror xorshift_state_tmp
	lsr xorshift_state_tmp+1
	ror xorshift_state_tmp
	lsr xorshift_state_tmp+1
	ror xorshift_state_tmp
	lsr xorshift_state_tmp+1
	lda xorshift_state_tmp
	ror
dfimaxy	cmp #24
	bpl dfiskipchar
dfiminy	cmp #$04
	bmi dfiskipchar

	tay
	
	jsr copyimgchar

dfiskipchar
	rts	

copyimgchar
	sty zp_tmp9
	tya
	clc
copyimg_img_addr_hi
	adc #>(pic2data+hcm_file_pic_data_start_offset)
	sta zp_tmp7+1
	tya
	clc
	adc #>display
	sta zp_tmp8+1

	jmp copyimg
after_copyimg

	ldy zp_tmp9
	jmp docopyul
after_docopyul
	
	ldy zp_tmp9

	rts

docopyul
	lda times8,y
	sta cpulfrom+1
	clc
	adc #$08
	sta cpulto+1

copyimg_ul1_addrs_hi
	lda copyul_pic2_ul1_hi,x
	sta zp_tmp2+1

copyimg_screen_ul1_addrs_hi
	lda copyul_screen_ul1_hi,x
	sta zp_tmp3+1

copyimg_ul2_addrs_hi
	lda copyul_pic2_ul2_hi,x
	sta zp_tmp4+1
	
copyimg_screen_ul2_addrs_hi
	lda copyul_screen_ul2_hi,x
	sta zp_tmp5+1

copyimg_mask_addr
	lda copyul_mask,x
	sta zp_tmp6
	
copyul
cpulfrom
	ldy #$00
cpull	lda (zp_tmp2),y
	sta cpuleor+1
	eor (zp_tmp3),y
	and zp_tmp6
cpuleor
	eor #$00
	sta (zp_tmp3),y

	lda (zp_tmp4),y
	sta cpul2eor+1
	eor (zp_tmp5),y
	and zp_tmp6
cpul2eor
	eor #$00
	sta (zp_tmp5),y
	iny
cpulto
	cpy #$08
	bne cpull

	jmp after_docopyul

copyimg
	ldy #$02
	lda (zp_tmp7),y
	sta (zp_tmp8),y
	ldy #$22
	lda (zp_tmp7),y
	sta (zp_tmp8),y
	ldy #$42
	lda (zp_tmp7),y
	sta (zp_tmp8),y
	ldy #$62
	lda (zp_tmp7),y
	sta (zp_tmp8),y
	ldy #$82
	lda (zp_tmp7),y
	sta (zp_tmp8),y
	ldy #$a2
	lda (zp_tmp7),y
	sta (zp_tmp8),y
	ldy #$c2
	lda (zp_tmp7),y
	sta (zp_tmp8),y
	ldy #$e2
	lda (zp_tmp7),y
	sta (zp_tmp8),y
	jmp after_copyimg

putimgchar
	stx zp_tmp8
	sty zp_tmp9
	tya
	clc
	adc #>display
	sta zp_tmp8+1

	lda #$00
	jmp putimg
after_putimg

	ldy zp_tmp9
	jmp doputulcolor
after_doputulcolor
	
	ldy zp_tmp9

	jmp after_putimgchar

putimg
	ldy #$02
	sta (zp_tmp8),y
	ldy #$22
	sta (zp_tmp8),y
	ldy #$42
	sta (zp_tmp8),y
	ldy #$62
	sta (zp_tmp8),y
	ldy #$82
	sta (zp_tmp8),y
	ldy #$a2
	sta (zp_tmp8),y
	ldy #$c2
	sta (zp_tmp8),y
	ldy #$e2
	sta (zp_tmp8),y
	jmp after_putimg

doputulcolor
	lda times8,y
	sta pulcfrom+1
	clc
	adc #$08
	sta pulcto+1

putimg_screen_ul1_addrs_hi
	lda copyul_screen_ul1_hi,x
	sta zp_tmp3+1
putimg_screen_ul2_addrs_hi
	lda copyul_screen_ul2_hi,x
	sta zp_tmp5+1

putimg_mask_addr
	lda copyul_mask,x
	sta zp_tmp6

putulcolor
pulcfrom
	ldy #$00
pulcl
	lda (zp_tmp3),y
	and zp_tmp6
	sta (zp_tmp3),y

	lda (zp_tmp5),y
	and zp_tmp6
	sta (zp_tmp5),y
	
	iny
pulcto
	cpy #$08
	bne pulcl
	jmp after_doputulcolor

copyul_mask
	.byte %01111111,%10111111,%11011111,%11101111,%11110111,%11111011,%11111101,%11111110
	.byte %01111111,%10111111,%11011111,%11101111,%11110111,%11111011,%11111101,%11111110
	.byte %11011101,%11101110,%01110111,%10111011
	.byte %01111111,%10111111,%11011111,%11101111,%11110111,%11111011,%11111101,%11111110

copyul_nonmix_mask
	.byte %01111111,%10111111,%11011111,%11101111,%11110111,%11111011,%11111101,%11111110
	.byte %01111111,%10111111,%11011111,%11101111,%11110111,%11111011,%11111101,%11111110
	.byte %11110101,%11111010,%01011111,%10101111
	.byte %01111111,%10111111,%11011111,%11101111,%11110111,%11111011,%11111101,%11111110
	
copyul_screen_ul1_hi
	.byte >(sprite_start+$420),>(sprite_start+$420),>(sprite_start+$420),>(sprite_start+$420)
	.byte >(sprite_start+$420),>(sprite_start+$420),>(sprite_start+$420),>(sprite_start+$420)
	.byte >(sprite_start+$520),>(sprite_start+$520),>(sprite_start+$520),>(sprite_start+$520)
	.byte >(sprite_start+$520),>(sprite_start+$520),>(sprite_start+$520),>(sprite_start+$520)
	.byte >(sprite_start+$320),>(sprite_start+$320),>(sprite_start+$320),>(sprite_start+$320)
	.byte >(sprite_start+$020),>(sprite_start+$020),>(sprite_start+$020),>(sprite_start+$020)
	.byte >(sprite_start+$020),>(sprite_start+$020),>(sprite_start+$020),>(sprite_start+$020)

copyul_screen_ul2_hi
	.byte >(sprite_start+$620),>(sprite_start+$620),>(sprite_start+$620),>(sprite_start+$620)
	.byte >(sprite_start+$620),>(sprite_start+$620),>(sprite_start+$620),>(sprite_start+$620)
	.byte >(sprite_start+$720),>(sprite_start+$720),>(sprite_start+$720),>(sprite_start+$720)
	.byte >(sprite_start+$720),>(sprite_start+$720),>(sprite_start+$720),>(sprite_start+$720)
	.byte >(sprite_start+$320),>(sprite_start+$320),>(sprite_start+$320),>(sprite_start+$320)
	.byte >(sprite_start+$120),>(sprite_start+$120),>(sprite_start+$120),>(sprite_start+$120)
	.byte >(sprite_start+$120),>(sprite_start+$120),>(sprite_start+$120),>(sprite_start+$120)
	
copyul_screen_nonmix_ul1_hi
	.byte >(sprite_start+$420),>(sprite_start+$420),>(sprite_start+$420),>(sprite_start+$420)
	.byte >(sprite_start+$420),>(sprite_start+$420),>(sprite_start+$420),>(sprite_start+$420)
	.byte >(sprite_start+$620),>(sprite_start+$620),>(sprite_start+$620),>(sprite_start+$620)
	.byte >(sprite_start+$620),>(sprite_start+$620),>(sprite_start+$620),>(sprite_start+$620)
	.byte >(sprite_start+$320),>(sprite_start+$320),>(sprite_start+$320),>(sprite_start+$320)
	.byte >(sprite_start+$020),>(sprite_start+$020),>(sprite_start+$020),>(sprite_start+$020)
	.byte >(sprite_start+$020),>(sprite_start+$020),>(sprite_start+$020),>(sprite_start+$020)

copyul_screen_nonmix_ul2_hi
	.byte >(sprite_start+$520),>(sprite_start+$520),>(sprite_start+$520),>(sprite_start+$520)
	.byte >(sprite_start+$520),>(sprite_start+$520),>(sprite_start+$520),>(sprite_start+$520)
	.byte >(sprite_start+$720),>(sprite_start+$720),>(sprite_start+$720),>(sprite_start+$720)
	.byte >(sprite_start+$720),>(sprite_start+$720),>(sprite_start+$720),>(sprite_start+$720)
	.byte >(sprite_start+$320),>(sprite_start+$320),>(sprite_start+$320),>(sprite_start+$320)
	.byte >(sprite_start+$120),>(sprite_start+$120),>(sprite_start+$120),>(sprite_start+$120)
	.byte >(sprite_start+$120),>(sprite_start+$120),>(sprite_start+$120),>(sprite_start+$120)

copyul_pic1_ul1_hi
	.byte >(pic1data+$420),>(pic1data+$420),>(pic1data+$420),>(pic1data+$420)
	.byte >(pic1data+$420),>(pic1data+$420),>(pic1data+$420),>(pic1data+$420)
	.byte >(pic1data+$520),>(pic1data+$520),>(pic1data+$520),>(pic1data+$520)
	.byte >(pic1data+$520),>(pic1data+$520),>(pic1data+$520),>(pic1data+$520)
	.byte >(pic1data+$320),>(pic1data+$320),>(pic1data+$320),>(pic1data+$320)
	.byte >(pic1data+$020),>(pic1data+$020),>(pic1data+$020),>(pic1data+$020)
	.byte >(pic1data+$020),>(pic1data+$020),>(pic1data+$020),>(pic1data+$020)

copyul_pic1_ul2_hi
	.byte >(pic1data+$620),>(pic1data+$620),>(pic1data+$620),>(pic1data+$620)
	.byte >(pic1data+$620),>(pic1data+$620),>(pic1data+$620),>(pic1data+$620)
	.byte >(pic1data+$720),>(pic1data+$720),>(pic1data+$720),>(pic1data+$720)
	.byte >(pic1data+$720),>(pic1data+$720),>(pic1data+$720),>(pic1data+$720)
	.byte >(pic1data+$320),>(pic1data+$320),>(pic1data+$320),>(pic1data+$320)
	.byte >(pic1data+$120),>(pic1data+$120),>(pic1data+$120),>(pic1data+$120)
	.byte >(pic1data+$120),>(pic1data+$120),>(pic1data+$120),>(pic1data+$120)

copyul_pic2_ul1_hi
	.byte >(pic2data+$420),>(pic2data+$420),>(pic2data+$420),>(pic2data+$420)
	.byte >(pic2data+$420),>(pic2data+$420),>(pic2data+$420),>(pic2data+$420)
	.byte >(pic2data+$520),>(pic2data+$520),>(pic2data+$520),>(pic2data+$520)
	.byte >(pic2data+$520),>(pic2data+$520),>(pic2data+$520),>(pic2data+$520)
	.byte >(pic2data+$320),>(pic2data+$320),>(pic2data+$320),>(pic2data+$320)
	.byte >(pic2data+$020),>(pic2data+$020),>(pic2data+$020),>(pic2data+$020)
	.byte >(pic2data+$020),>(pic2data+$020),>(pic2data+$020),>(pic2data+$020)

copyul_pic2_ul2_hi
	.byte >(pic2data+$620),>(pic2data+$620),>(pic2data+$620),>(pic2data+$620)
	.byte >(pic2data+$620),>(pic2data+$620),>(pic2data+$620),>(pic2data+$620)
	.byte >(pic2data+$720),>(pic2data+$720),>(pic2data+$720),>(pic2data+$720)
	.byte >(pic2data+$720),>(pic2data+$720),>(pic2data+$720),>(pic2data+$720)
	.byte >(pic2data+$320),>(pic2data+$320),>(pic2data+$320),>(pic2data+$320)
	.byte >(pic2data+$120),>(pic2data+$120),>(pic2data+$120),>(pic2data+$120)
	.byte >(pic2data+$120),>(pic2data+$120),>(pic2data+$120),>(pic2data+$120)
	
copyul_pic3_ul1_hi
	.byte >(pic3data+$420),>(pic3data+$420),>(pic3data+$420),>(pic3data+$420)
	.byte >(pic3data+$420),>(pic3data+$420),>(pic3data+$420),>(pic3data+$420)

	.byte >(pic3data+$620),>(pic3data+$620),>(pic3data+$620),>(pic3data+$620)
	.byte >(pic3data+$620),>(pic3data+$620),>(pic3data+$620),>(pic3data+$620)

	.byte >(pic3data+$320),>(pic3data+$320),>(pic3data+$320),>(pic3data+$320)

	.byte >(pic3data+$020),>(pic3data+$020),>(pic3data+$020),>(pic3data+$020)
	.byte >(pic3data+$020),>(pic3data+$020),>(pic3data+$020),>(pic3data+$020)

copyul_pic3_ul2_hi
	.byte >(pic3data+$520),>(pic3data+$520),>(pic3data+$520),>(pic3data+$520)
	.byte >(pic3data+$520),>(pic3data+$520),>(pic3data+$520),>(pic3data+$520)

	.byte >(pic3data+$720),>(pic3data+$720),>(pic3data+$720),>(pic3data+$720)
	.byte >(pic3data+$720),>(pic3data+$720),>(pic3data+$720),>(pic3data+$720)

	.byte >(pic3data+$320),>(pic3data+$320),>(pic3data+$320),>(pic3data+$320)

	.byte >(pic3data+$120),>(pic3data+$120),>(pic3data+$120),>(pic3data+$120)
	.byte >(pic3data+$120),>(pic3data+$120),>(pic3data+$120),>(pic3data+$120)

initsprites
	lda #>sprite_start
	sta PMBASE
	
	lda #$00
	sta PRIOR
	sta ldprior+1

	ldy #$07
	lda #$ff
ispr0	sta HPOSP0,y
	dey
	bpl ispr0

	lda #$03
	sta SIZEP0
	sta SIZEP1
	sta SIZEP2
	sta SIZEP3
	lda #$ff
	sta SIZEM

	lda #$03
	sta GRACTL
	
	rts
	
cleardisplay
	lda #$00
	ldy #$20
cst2	ldx #$00
cst1	sta sprite_start, x
	inx
	bne cst1
	inc cst1+2
	dey
	bne cst2
	rts	

genreadyanim_bridgehcm
	lda #$55
	ldx #$00
gbh1	ldy #$07
gbh0	sta sprite_start+$440,x
	sta sprite_start+$540,x
	sta sprite_start+$40,x
	eor #$f0
	sta sprite_start+$340,x
	eor #$0f
	sta sprite_start+$640,x
	sta sprite_start+$740,x
	sta sprite_start+$140,x
	eor #$ff
	inx
	dey
	bpl gbh0
	eor #$ff
	cpx #$80
	bne gbh1
	rts
	
initsound
	jsr initfilter

	.ifndef debugtimers
	ldx #$00
	lda #$10
imt0	sta samplebuf, x
	sta samplebuf2, x
	inx
	cpx #num_samples
	bne imt0
	rts
	.else
	ldx #$00
	lda #$10
	
imt1	eor #$0f
	sta samplebuf, x
	;eor #$0f
	sta samplebuf2, x
	;eor #$0f
	inx
	cpx #num_samples
	bne imt1
	rts	
	.endif
	
dosound
	ldx #$100-num_samples
dshist	ldy #0 ;reading history from last round
		
dsnext

dsaccl	lda #$00 ;accu lo
	clc ;todo: review/remove - might not need this
dsincl	adc #$00 ;increment lo
	sta dsaccl+1
	
dsacch	lda #$00 ;accu hi
dsinch	adc #$00 ;increment hi
	sta dsacch+1
		
dspw	cmp #$00 ;pulse width
		
dsflttbl
	lda cutoff_scale_old+$00,y ;scale history
	bcc dsstor ;see prev cmp
		
dshim1	adc #$00 ;C is always set here! make sure to store value-1
		
dsstor	tay ;put result into history
	ora #$10
dsbuff	sta samplebuf-($100-num_samples),x

	inx
	bne dsnext
	
	sty dshist+1 ;persist history for next round

	;swap buffers
	lda dsbuff+2
	cmp #>(samplebuf-($100-num_samples))
	beq setsoundbuff2
	
setsoundbuff1
	lda #<(samplebuf-($100-num_samples))
	sta dsbuff+1
	lda #>(samplebuf-($100-num_samples))
	sta dsbuff+2
	rts
	
setsoundbuff2
	lda #<(samplebuf2-($100-num_samples))
	sta dsbuff+1
	lda #>(samplebuf2-($100-num_samples))
	sta dsbuff+2
	rts

initfilter
	lda #<cutoff_scale_old
	sta cutofftbl+1
	lda #>cutoff_scale_old
	sta cutofftbl+2
	
	ldx #$00
	lda #cutoff_margin
if0	sta cutofftarget+1
	pha
	txa
	pha

	jsr initcutoff
	
	pla
	tax
	pla
	clc
	adc #cutoff_step
	inx
	cpx #$10
	bne if0
	rts
	
initcutoff
	ldx #$00
	ldy #$00
	lda #$00
	clc
	
cutofftbl
	sty cutoff_scale_old

cutofftarget
	adc #$05 ;target value
	bcc icnext
	iny
	clc
	
icnext
	inc cutofftbl+1
	inx
	cpx #$10
	bne cutofftbl
	rts
	
inittracker
	lda #$00
	sta instrFlags
	sta baseinclo+1
	sta baseinchi+1
	sta basecutoff+1
	sta basepw+1
	sta pwlfo_value+1
	sta pwlfo_inc+1
	sta pwlfo_depth+1
	sta dotracker_pwlfo_reverse_on_off+1
	sta cutofflfo_value+1
	sta cutofflfo_inc+1
	sta cutofflfo_depth+1
	sta dotracker_cutofflfo_reverse_on_off+1
	sta vib_value_lo+1
	sta vib_value_hi+1
	sta vib_inc_lo+1
	sta vib_inc_hi+1
	sta vib_depth+1
	sta dotracker_vib_lo_reverse_on_off+1
	sta dotracker_vib_hi_reverse_on_off+1
	
	.ifndef debugtimers
	.ifdef debugtracker
	lda #$ff
	sta trackerframe
	.endif
	.endif

	lda #$01
	sta pattern_skip_frames
		
	lda #$80
	sta dotracker_porta_on_off+1

	//TODO: Support no-loop ($ff)
	lda #<hbassdata
	clc
	adc song_loop_pos_addr
	sta loop_addr_lo+1
	lda #>hbassdata
	adc #$00
	sta loop_addr_hi+1
	
	lda #<note_tbl_lo_addr
	clc
	adc note_count_addr
	sta note_tbl_hi_addr+1
	lda #>note_tbl_lo_addr
	adc #$00
	sta note_tbl_hi_addr+2

	lda note_count_addr
	asl
	clc
	adc #<note_tbl_lo_addr
	sta pattern_index_cursor
	sta zp_tmp
	sta max_pattern_addr_addr+1
	lda #>note_tbl_lo_addr
	adc #$00
	sta pattern_index_cursor+1
	sta zp_tmp+1
	sta max_pattern_addr_addr+2
	
	ldx #$00
max_pattern_addr_addr
	lda $1111,x
	sta max_pattern_addr,x
	inx
	cpx #$02
	bne max_pattern_addr_addr
	
	lda max_pattern_addr
	clc
	adc #<hbassdata
	sta max_pattern_addr
	lda max_pattern_addr+1
	adc #>hbassdata
	sta max_pattern_addr+1

pattern_addr_reloc
	ldy #$00
	lda (zp_tmp),y
	clc
	adc #<hbassdata
	sta (zp_tmp),y
	iny
	lda (zp_tmp),y
	adc #>hbassdata
	sta (zp_tmp),y
	lda zp_tmp
	clc
	adc #$02
	sta zp_tmp
	bcc pattern_addr_reloc0
	inc zp_tmp+1
pattern_addr_reloc0
	lda zp_tmp
	cmp max_pattern_addr
	bne pattern_addr_reloc
	lda zp_tmp+1
	cmp max_pattern_addr+1
	bne pattern_addr_reloc
	
	jsr loadpattern
	
	ldx #$00
	ldy #$ff
filter_tbl_helper_loop	
	lda cutoff_scale_old,y
	eor #$0f
	sec
	sbc #$01
	cmp #max_pulse_inc
	bcc finrange
	lda #max_pulse_inc
finrange
	sta filter_dshim1s,x
	tya
	sec
	sbc #$10
	tay
	inx
	cpx #$10
	bne filter_tbl_helper_loop
	
	rts

loadpattern
	ldy #$00
	lda (pattern_index_cursor),y
	sta pattern_data_cursor
	iny
	lda (pattern_index_cursor),y
	sta pattern_data_cursor+1
	
	lda pattern_index_cursor
	clc
	adc #$02
	sta pattern_index_cursor
	bcc loadpattern0
	inc pattern_index_cursor+1
loadpattern0
	lda pattern_index_cursor
	cmp max_pattern_addr
	bne loadpatternend
	lda pattern_index_cursor+1
	cmp max_pattern_addr+1
	bne loadpatternend
	
	//TODO: Support no-loop ($ff)

loop_addr_lo
	lda #$00
	sta pattern_index_cursor
loop_addr_hi
	lda #$00
	sta pattern_index_cursor+1
	
loadpatternend
	rts
		
dotracker
	.ifdef debugtracker
	inc trackerframe	
	.endif

	lda pattern_skip_frames
	cmp #$01
	beq dotracker_process_1st_pattern_command
	dec pattern_skip_frames
	jmp dotracker_after_cmd_proc

dotracker_process_1st_pattern_command
	ldy #$00
dotracker_process_next_pattern_command
	lda (pattern_data_cursor),y
	cmp #$80
	beq dotracker_endpattern
	bpl dotracker_hasskipfr
	tax
	lda #$00
	sta dotracker_process_command_skipfr+1
	lda pattern_commands_lo,x
	sta dotracker_cmd_jump2+1
	lda pattern_commands_hi,x
	sta dotracker_cmd_jump2+2
dotracker_cmd_jump2
	jmp $1111
	
dotracker_endpattern
	jsr loadpattern
	jmp dotracker_process_1st_pattern_command
	
dotracker_hasskipfr	
	tax
	and #$78
	lsr	
	lsr	
	lsr
	sta dotracker_process_command_skipfr+1
	txa
	and #$07
	tax
	lda pattern_commands_lo,x
	sta dotracker_cmd_jump+1
	lda pattern_commands_hi,x
	sta dotracker_cmd_jump+2
dotracker_cmd_jump
	jmp $1111	
	
dotracker_cmd_0 ;EXPORT_COMMAND_SKIP_FRAMES
	iny
	lda (pattern_data_cursor),y
	sta dotracker_process_command_skipfr+1

	jmp dotracker_aftercmd	
	
dotracker_cmd_1 ;EXPORT_COMMAND_NOTE_CHANGE
	iny
	lda (pattern_data_cursor),y
	bmi set_note_vib
	
	tax
	lda note_tbl_lo_addr,x
	sta baseinclo+1
note_tbl_hi_addr
	lda $1111,x
	sta baseinchi+1

	lda instrFlags
	and #$40
	beq dotracker_after_set_note_vib
	
set_note_vib
	iny
	lda (pattern_data_cursor),y
	sta vib_depth+1
	iny
	lda (pattern_data_cursor),y
	sta vib_inc_lo+1
	lda #$00
	sta vib_inc_hi+1

dotracker_after_set_note_vib
	jmp dotracker_aftercmd	
	
dotracker_cmd_2 ;EXPORT_COMMAND_FLAGS_CHANGE
	ldx instrFlags
	iny
	lda (pattern_data_cursor),y
	sta instrFlags
	bmi turnSoundOn
	
turnSoundOff
	txa
	bpl sound_already_off
	
	lda #$00
	sta dsincl+1
	sta dsinch+1
	
sound_already_off
	jmp dotracker_aftercmd

turnSoundOn
	txa
	bmi sound_already_on

	lda #$00
	sta pwlfo_value+1
	sta dotracker_pwlfo_reverse_on_off+1
	sta cutofflfo_value+1
	sta dotracker_cutofflfo_reverse_on_off+1
	sta vib_value_lo+1
	sta vib_value_hi+1
	sta dotracker_vib_lo_reverse_on_off+1
	sta dotracker_vib_hi_reverse_on_off+1

sound_already_on
	jmp dotracker_aftercmd	
		
dotracker_cmd_3; EXPORT_COMMAND_INSTRUMENT_CHANGE
	lda #$00
	sta instrFlags
	sta pwlfo_value+1
	sta dotracker_pwlfo_reverse_on_off+1
	sta cutofflfo_value+1
	sta dotracker_cutofflfo_reverse_on_off+1
	sta vib_value_lo+1
	sta vib_value_hi+1
	sta dotracker_vib_lo_reverse_on_off+1
	sta dotracker_vib_hi_reverse_on_off+1
	
	iny
	lda (pattern_data_cursor),y
	sta pwlfo_depth+1
	iny
	lda (pattern_data_cursor),y
	sta pwlfo_inc+1
	iny
	lda (pattern_data_cursor),y
	sta cutofflfo_depth+1
	iny
	lda (pattern_data_cursor),y
	sta cutofflfo_inc+1

	jmp dotracker_aftercmd	
	
dotracker_cmd_4; EXPORT_COMMAND_CUTOFF_CHANGE
	iny
	lda (pattern_data_cursor),y
	sta basecutoff+1

	jmp dotracker_aftercmd	
	
dotracker_cmd_5; EXPORT_COMMAND_PW_CHANGE
	iny
	lda (pattern_data_cursor),y
	sta basepw+1

	jmp dotracker_aftercmd	
	
dotracker_cmd_6; EXPORT_COMMAND_PORTAMENTO_STAGE_START_FROM_FREQ
	lda #$01
	sta dotracker_porta_on_off+1
	iny
	lda (pattern_data_cursor),y
	sta baseinclo+1
	iny
	lda (pattern_data_cursor),y
	sta baseinchi+1
	iny
	lda (pattern_data_cursor),y
	sta portaddinclo+1
	iny
	lda (pattern_data_cursor),y
	sta portaddinchi+1
	
	jmp dotracker_aftercmd	
	
dotracker_cmd_7; EXPORT_COMMAND_PORTAMENTO_END
	lda #$80
	sta dotracker_porta_on_off+1

	jmp dotracker_aftercmd	
	
dotracker_aftercmd
	iny
	
dotracker_process_command_skipfr
	lda #$00
	bne dotracker_store_skipfr
	jmp dotracker_process_next_pattern_command
dotracker_store_skipfr	
	sta pattern_skip_frames

	tya
	ldy #$00
	clc
	adc pattern_data_cursor
	sta pattern_data_cursor
	bcc dotracker_after_cmd_proc
	inc pattern_data_cursor+1

dotracker_after_cmd_proc

dotracker_porta_on_off
	lda #$00
	beq dotracker_process_porta
	bmi dotracker_no_porta
	dec dotracker_porta_on_off+1
	jmp dotracker_no_porta

dotracker_process_porta

portaddinclo
	lda #$00
	clc
	adc baseinclo+1
	sta baseinclo+1
	
portaddinchi
	lda #$00
	adc baseinchi+1
	sta baseinchi+1

dotracker_no_porta	

	//process LFOs / Vibrato

	lda instrFlags
	and #$10
	beq dotracker_nopwlfo

	//process PW LFO
pwlfo_value
	lda #$00
	clc
pwlfo_inc
	adc #$00
	sta pwlfo_value+1
	bne pwlfo_check_range
	lda dotracker_pwlfo_reverse_on_off+1
	eor #$ff
	sta dotracker_pwlfo_reverse_on_off+1	
	jmp dotracker_pwlfo_reverse

pwlfo_check_range
	
pwlfo_depth
	cmp #$00
	bcc dotracker_pwlfo_within_range

dotracker_pwlfo_reverse	
	lda pwlfo_inc+1
	eor #$ff
	adc #$00 ;C is set already
	sta pwlfo_inc+1
	
dotracker_pwlfo_within_range

dotracker_nopwlfo



	lda instrFlags
	and #$20
	beq dotracker_nocutofflfo

	//process cutoff LFO
cutofflfo_value
	lda #$00
	clc
cutofflfo_inc
	adc #$00
	sta cutofflfo_value+1
	bne cutofflfo_check_range
	lda dotracker_cutofflfo_reverse_on_off+1
	eor #$ff
	sta dotracker_cutofflfo_reverse_on_off+1	
	jmp dotracker_cutofflfo_reverse

cutofflfo_check_range
	
cutofflfo_depth
	cmp #$00
	bcc dotracker_cutofflfo_within_range

dotracker_cutofflfo_reverse	
	lda cutofflfo_inc+1
	eor #$ff
	adc #$00 ;C is set already
	sta cutofflfo_inc+1
	
dotracker_cutofflfo_within_range

dotracker_nocutofflfo




	lda instrFlags
	bmi dotracker_sound_is_on
	jmp dotracker_nosound	

dotracker_sound_is_on

	and #$40
	beq dotracker_novib

	//process vibrato

vib_value_lo
	lda #$00
	clc
vib_inc_lo
	adc #$00
	sta vib_value_lo+1
vib_value_hi
	lda #$00
vib_inc_hi
	adc #$00
	sta vib_value_hi+1
	cmp #$ff
	bne vib_check_range
	lda dotracker_vib_lo_reverse_on_off+1
	eor #$ff
	sta dotracker_vib_lo_reverse_on_off+1	
	lda dotracker_vib_hi_reverse_on_off+1
	eor #$ff
	sta dotracker_vib_hi_reverse_on_off+1	
	jmp dotracker_vib_reverse

vib_check_range
	lda vib_value_hi+1
	cmp #$00
	bcc dotracker_vib_within_range
	bne dotracker_vib_reverse
	lda vib_value_lo+1
vib_depth
	cmp #$00
	bcc dotracker_vib_within_range
	
dotracker_vib_reverse	
	lda vib_inc_lo+1
	eor #$ff
	adc #$00 ;C is set already
	sta vib_inc_lo+1
	lda vib_inc_hi+1
	eor #$ff
	sta vib_inc_hi+1
	
dotracker_vib_within_range

dotracker_novib

	//update sound registers

	lda vib_value_lo+1
dotracker_vib_lo_reverse_on_off
	eor #$00
	clc
baseinclo
	adc #$00
	sta dsincl+1

	lda vib_value_hi+1
dotracker_vib_hi_reverse_on_off
	eor #$00
baseinchi
	adc #$00
	sta dsinch+1

	
	lda pwlfo_value+1
dotracker_pwlfo_reverse_on_off
	eor #$00
	clc
basepw
	adc #$00
	sta dspw+1


	lda cutofflfo_value+1
dotracker_cutofflfo_reverse_on_off
	eor #$00
	clc
basecutoff
	adc #$00
	tax
	lda filter_tbls,x
	sta dsflttbl+1
	lda filter_dshim1s,x
	sta dshim1+1
	
dotracker_nosound
	
	rts

;RMT will call this when needed
HARDBASS_SOUND_ENABLE
	nop
	lda #$60
	sta HARDBASS_SOUND_ENABLE ;rts
	lda #$ea
	sta HARDBASS_SOUND_DISABLE ;nop

	lda #$8c ;sty abs
	sta styaudc
	sta styaudc2
	lda #$8e ;stx abs
	sta stxaudf
	lda #$8d ;sta abs
	sta staaudf
	sta staaudf2
	sta staaudc
	lda #timer
	sta timerena
	rts

;RMT will call this when needed
HARDBASS_SOUND_DISABLE
	nop
	lda #$60
	sta HARDBASS_SOUND_DISABLE ;rts
	lda #$ea
	sta HARDBASS_SOUND_ENABLE ;nop

	lda #$00
	sta IRQEN
	
	lda #$ac ;ldy abs
	sta styaudc
	sta styaudc2
	sta stxaudf ;we store ldy here too, ldx wouldn't work
	lda #$ad ;lda abs
	sta staaudf
	sta staaudf2
	sta staaudc
	lda #$00
	sta timerena
	rts

	;todo: remove this copy stuff and load straight into the zero page
copyzp	ldy #0
zpcopy 	mva .adr(zpcode),y zpcode,y+
	cpy #.len zpcode
	bne zpcopy
	rts

	.local zpcode, zpvarsend
	
irq	sta isa+1
	lda #0
	sta irqen

irqsplcnt
sb2	lda samplebuf+$18 ;this is page start as the buffer is aligned to xxe8
	sta sample_register

	.ifdef debugtimers
	and #$e0
	beq debugok
	nop ;wrong value - place breakpoint here to notice
debugok
	.endif

	inc irqsplcnt+1

irqtpt
	lda timerpattern+5
	sta AUDF1+(timer-1)*2
	dec irqtpt+1
	bpl irqmorept
	lda #timerpatternmax
	sta irqtpt+1
	
irqmorept
	
	lda #timer
	sta irqen

isa	lda #0
	rti
	
acpycnt .ds 0

	.endl

max_pattern_addr
	.ds 2
	
pattern_skip_frames
	.ds 1
	
	.ifdef debugtracker
trackerframe
	.ds 1
	.endif

pattern_commands_lo
	.byte <dotracker_cmd_0,<dotracker_cmd_1,<dotracker_cmd_2,<dotracker_cmd_3
	.byte <dotracker_cmd_4,<dotracker_cmd_5,<dotracker_cmd_6,<dotracker_cmd_7

pattern_commands_hi
	.byte >dotracker_cmd_0,>dotracker_cmd_1,>dotracker_cmd_2,>dotracker_cmd_3
	.byte >dotracker_cmd_4,>dotracker_cmd_5,>dotracker_cmd_6,>dotracker_cmd_7

hbassdata	
	INS "export.hardbass"

	song_loop_pos_addr = hbassdata+8
	note_count_addr = hbassdata+9
	note_tbl_lo_addr = hbassdata+10

	.align $400
tsdata	INS "tsanim.dat"
	
	ts_char_data = tsdata
	ts_anim_data = tsdata+$400
	
?smcnt = 0

	?smcnt++\ CallFor ?smcnt $400 dofadein

	?smcnt++\ CallFor ?smcnt 96

	?smcnt++\ CallFor ?smcnt 1 showrrbottom
	?smcnt++\ CallFor ?smcnt 25
	?smcnt++\ CallFor ?smcnt 1 stoprrr
	
	?smcnt++\ CallFor ?smcnt 381
	?smcnt++\ CallFor ?smcnt 1 ffwscroller +startrrs

	?smcnt++\ CallFor ?smcnt 31 ffwscroller
	?smcnt++\ CallFor ?smcnt $30
	?smcnt++\ CallFor ?smcnt 48 ffwscroller
	?smcnt++\ CallFor ?smcnt $30
	?smcnt++\ CallFor ?smcnt 48 ffwscroller
	?smcnt++\ CallFor ?smcnt $30
	?smcnt++\ CallFor ?smcnt 48 ffwscroller

	?smcnt++\ CallFor ?smcnt $30
	?smcnt++\ CallFor ?smcnt 48 ffwscroller
	?smcnt++\ CallFor ?smcnt $30
	?smcnt++\ CallFor ?smcnt 48 ffwscroller
	?smcnt++\ CallFor ?smcnt $30
	?smcnt++\ CallFor ?smcnt 48 ffwscroller

	?smcnt++\ CallFor ?smcnt $60
	?smcnt++\ CallFor ?smcnt 48 ffwscroller

	?smcnt++\ CallFor ?smcnt $160
	
	?smcnt++\ CallFor ?smcnt 48 ffwscroller
	
	?smcnt++\ CallFor ?smcnt $310
	
	?smcnt++\ CallFor ?smcnt 32 ffwscroller
	
	?smcnt++\ CallFor ?smcnt 1 startrrr
	?smcnt++\ CallFor ?smcnt 37 ;spin until the thinnest position
	?smcnt++\ CallFor ?smcnt 1 stoprrr hiderr

	?smcnt++\ CallFor ?smcnt 1 initblackstripes
	?smcnt++\ CallFor ?smcnt $10f dostripes
	?smcnt++\ CallFor ?smcnt 1 initfadein_pic2
	?smcnt++\ CallFor ?smcnt $100 dofadeinquick
	?smcnt++\ CallFor ?smcnt 1 initswapouthcmpart1
	?smcnt++\ CallFor ?smcnt 23 swapouthcmparts swapouthcmparts swapouthcmparts swapouthcmparts
	?smcnt++\ CallFor ?smcnt 1 initswapouthcmpart2
	?smcnt++\ CallFor ?smcnt 20 swapouthcmparts swapouthcmparts swapouthcmparts swapouthcmparts

	?smcnt++\ CallFor ?smcnt $7b

	?smcnt++\ CallFor ?smcnt 1 moverrpersine showrr startrrr
	?smcnt++\ CallFor ?smcnt 25 ;spin back to normal position
	?smcnt++\ CallFor ?smcnt 1 stoprrr

	?smcnt++\ CallFor ?smcnt $1c0 moverrpersine ;consecutive full spins

	?smcnt++\ CallFor ?smcnt 1 startrrr moverrpersine
	?smcnt++\ CallFor ?smcnt $3f moverrpersine ;1st full spin after start

	?smcnt++\ CallFor ?smcnt $a4 moverrpersine
	
	?smcnt++\ CallFor ?smcnt 1 moverrpersine initswapouthcmpart1
	?smcnt++\ CallFor ?smcnt 23 moverrpersine swapouthcmparts swapouthcmparts swapouthcmparts swapouthcmparts
	?smcnt++\ CallFor ?smcnt 1 moverrpersine initswapouthcmpart2
	?smcnt++\ CallFor ?smcnt 20 moverrpersine swapouthcmparts swapouthcmparts swapouthcmparts swapouthcmparts
	?smcnt++\ CallFor ?smcnt 1 moverrpersine initblackstripes
	?smcnt++\ CallFor ?smcnt $10f moverrpersine dostripes

	?smcnt++\ CallFor ?smcnt 31 moverrpersine ;spin back to normal position
	?smcnt++\ CallFor ?smcnt 1 stoprrr moverrpersine

	?smcnt++\ CallFor ?smcnt $30 moverrpersine

	?smcnt++\ CallFor ?smcnt 1 initfadein_pic3 initswapouthcmborders
	?smcnt++\ CallFor ?smcnt $30 swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders
	?smcnt++\ CallFor ?smcnt 1 pic3copychar00
	?smcnt++\ CallFor ?smcnt $100 dofadeinquick
	?smcnt++\ CallFor ?smcnt $100 recyclepic1data moverrpersine

	?smcnt++\ CallFor ?smcnt $4a6 moverrpersine
	?smcnt++\ CallFor ?smcnt $340

	?smcnt++\ CallFor ?smcnt 1 initblackstripes
	?smcnt++\ CallFor ?smcnt $10f dostripes
	?smcnt++\ CallFor ?smcnt 1 initswapouthcmborders
	?smcnt++\ CallFor ?smcnt $30 swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders swapouthcmborders

	?smcnt++\ CallFor ?smcnt 1 startrrr
	?smcnt++\ CallFor ?smcnt 37 ;spin until the thinnest position
	?smcnt++\ CallFor ?smcnt 1 stoprrr hiderr

	?smcnt++\ CallFor ?smcnt $1c
	
	?smcnt++\ CallFor ?smcnt 1 showtsscreen
	?smcnt++\ CallFor ?smcnt $4f opentsscreen
	?smcnt++\ CallFor ?smcnt 1 showtssprites
	?smcnt++\ CallFor ?smcnt $1f rollintssprites
	?smcnt++\ CallFor ?smcnt 1 stopstatemachine maintaintsscreen

	.def statemachinecnt = ?smcnt

	.ifdef debugstatemachine
smdummy
	rts
	.endif

smprocaddrs_lo
	.rept statemachinecnt, #
	.byte .GET[$8000+:1]
	.endr

smprocaddrs_hi
	.rept statemachinecnt, #
	.byte .GET[$8100+:1]
	.endr

smcnts_lo
	.rept statemachinecnt, #
	.byte .GET[$8200+:1]
	.endr

smcnts_hi
	.rept statemachinecnt, #
	.byte .GET[$8300+:1]
	.endr

	.ifdef debugstatemachine
smframe	.word 0
	.endif

dostatemachine

smindjsr
	jsr .GET[$8000]|(.GET[$8100]<<8)

	.ifdef debugstatemachine
	inc smframe
	bne smf0
	inc smframe+1
smf0
	.endif

	lda smcnt

	bne smnohidec
	lda smcnt+1
	beq smcntend
	dec smcnt+1

smnohidec
	dec smcnt
	rts

smcntend

dosm0	ldx #$00
dosm2	inx
	cpx #statemachinecnt
	bne dosm1
	ldx #$00

dosm1	stx dosm0+1

	lda smprocaddrs_lo,x
	sta smindjsr+1
	lda smprocaddrs_hi,x
	sta smindjsr+2

	lda smcnts_lo,x
	sta smcnt
	lda smcnts_hi,x
	sta smcnt+1

smrts	rts

stopstatemachine
	lda #$ea
	sta dosm2
	rts

smcnt	.byte .GET[$8200],.GET[$8300]

.macro CallFor cnt, repeat, calladdr, calladdr2, calladdr3, calladdr4, calladdr5, calladdr6, calladdr7, calladdr8

	.if :calladdr2!=$ffffffff

	.PUT[$8000+:cnt-1]=<*
	.PUT[$8100+:cnt-1]=>*
	.PUT[$8200+:cnt-1]=<(:repeat-1)
	.PUT[$8300+:cnt-1]=>(:repeat-1)

	.local
	.if :calladdr!=$ffffffff
	jsr :calladdr
	.endif
	.if :calladdr2!=$ffffffff
	jsr :calladdr2
	.endif
	.if :calladdr3!=$ffffffff
	jsr :calladdr3
	.endif
	.if :calladdr4!=$ffffffff
	jsr :calladdr4
	.endif
	.if :calladdr5!=$ffffffff
	jsr :calladdr5
	.endif
	.if :calladdr6!=$ffffffff
	jsr :calladdr6
	.endif
	.if :calladdr7!=$ffffffff
	jsr :calladdr7
	.endif
	.if :calladdr8!=$ffffffff
	jsr :calladdr8
	.endif
	rts
	.endl

	.else

	.if :calladdr!=$ffffffff
	.PUT[$8000+:cnt-1]=<:calladdr
	.PUT[$8100+:cnt-1]=>:calladdr
	.else
	.PUT[$8000+:cnt-1]=<:smrts
	.PUT[$8100+:cnt-1]=>:smrts
	.endif
	.PUT[$8200+:cnt-1]=<(:repeat-1)
	.PUT[$8300+:cnt-1]=>(:repeat-1)

	.endif

.endm

decstatemachinecnt
	rts

stmcnt	.ds 2

initswapouthcmpart1
	lda #89
	sta swphcm7+1
	lda #$00
	sta swphcm1+1

	lda #$05
	sta swphcm9+1
	lda #$04
	sta swphcm8+1

	lda #<hcmpart1
	sta swphcm0+1
	sta swphcm6+1
	lda #>hcmpart1
	sta swphcm0+2
	sta swphcm6+2

	lda #<(display+32*101)
	sta swphcm3+1
	sta swphcm4+1
	lda #>(display+32*101)
	sta swphcm3+2
	sta swphcm4+2

	lda #$00
	sta swphcma+1

	rts

initswapouthcmpart2
	lda #80
	sta swphcm7+1
	lda #$00
	sta swphcm1+1

	lda #$04
	sta swphcm9+1
	lda #$03
	sta swphcm8+1

	lda #<hcmpart2
	sta swphcm0+1
	sta swphcm6+1
	lda #>hcmpart2
	sta swphcm0+2
	sta swphcm6+2

	lda #<(display+32*2+28)
	sta swphcm3+1
	sta swphcm4+1
	lda #>(display+32*2+28)
	sta swphcm3+2
	sta swphcm4+2

	lda #$01
	sta swphcma+1

	rts

swapouthcmparts
swphcm1	ldx #$00
swphcm7	cpx #89
	beq swphcmb

swphcma	lda #$00
	beq swphcm8

	lda hcmpart2_sprite1,x
	ldy sprite_start+$22,x
	sta sprite_start+$22,x
	tya
	sta hcmpart2_sprite1,x

	lda hcmpart2_sprite2,x
	ldy sprite_start+$122,x
	sta sprite_start+$122,x
	tya
	sta hcmpart2_sprite2,x

swphcm8	ldx #$04
swphcm0
	lda hcmpart1,x
swphcm3
	ldy display+32*101,x
swphcm4
	sta display+32*101,x
	tya
swphcm6
	sta hcmpart1,x
	dex
	bpl swphcm0

	lda swphcm0+1
	clc
swphcm9	adc #$05
	sta swphcm0+1
	sta swphcm6+1
	bcc swphcm5
	inc swphcm0+2
	inc swphcm6+2

swphcm5

	lda swphcm3+1
	clc
	adc #$20
	sta swphcm3+1
	sta swphcm4+1
	bcc swphcm2
	inc swphcm3+2
	inc swphcm4+2

swphcm2
	inc swphcm1+1	

swphcmb
	rts

initswapouthcmborders
	lda #$00
	sta swphcb1+1
	
	lda #<(pic3data+hcm_file_pic_data_start_offset+32*191)
	sta swphcb0+1
	sta swphcb6+1
	lda #>(pic3data+hcm_file_pic_data_start_offset+32*191)
	sta swphcb0+2
	sta swphcb6+2
	
	lda #<(pic3data+hcm_file_pic_data_start_offset+30)
	sta swphcb0b+1
	sta swphcb6b+1
	lda #>(pic3data+hcm_file_pic_data_start_offset+30)
	sta swphcb0b+2
	sta swphcb6b+2

	lda #<(display+32*191)
	sta swphcb3+1
	sta swphcb4+1
	lda #>(display+32*191)
	sta swphcb3+2
	sta swphcb4+2

	lda #<(display+30)
	sta swphcb3b+1
	sta swphcb4b+1
	lda #>(display+30)
	sta swphcb3b+2
	sta swphcb4b+2

	rts

swapouthcmborders
swphcb1	ldx #$00
	cpx #$c0
	beq swphcbb

	ldx #$01

swphcb0
	lda pic3data+hcm_file_pic_data_start_offset+32*191,x
swphcb3
	ldy display+32*191,x
swphcb4
	sta display+32*191,x
	tya
swphcb6
	sta pic3data+hcm_file_pic_data_start_offset+32*191,x

swphcb0b
	lda pic3data+hcm_file_pic_data_start_offset+30,x
swphcb3b
	ldy display+30,x
swphcb4b
	sta display+30,x
	tya
swphcb6b
	sta pic3data+hcm_file_pic_data_start_offset+30,x

	dex
	bpl swphcb0

	lda swphcb0+1
	sec
	sbc #$20
	sta swphcb0+1
	sta swphcb6+1
	bcs swphcb5
	dec swphcb0+2
	dec swphcb6+2

swphcb5

	lda swphcb3+1
	sec
	sbc #$20
	sta swphcb3+1
	sta swphcb4+1
	bcs swphcb2
	dec swphcb3+2
	dec swphcb4+2

swphcb2

	lda swphcb0b+1
	clc
	adc #$20
	sta swphcb0b+1
	sta swphcb6b+1
	bcc swphcb5b
	inc swphcb0b+2
	inc swphcb6b+2

swphcb5b

	lda swphcb3b+1
	clc
	adc #$20
	sta swphcb3b+1
	sta swphcb4b+1
	bcc swphcb2b
	inc swphcb3b+2
	inc swphcb4b+2

swphcb2b

	inc swphcb1+1	

swphcbb
	rts

	.if *>=$D000
	;address check based on advise from TeBe
	ert "Can not compile under D000-D800! Address: "*
	.else
	.print "Space left before D000: ",$d000-*," (address: ",*,")"
	.endif

	;skipping RMT music data at $d800	

	org rmt_music_data_end

scrfont_col1
	INS "scrollerfont.dat"

	scrfont_col2 = scrfont_col1+$400
	
rrdldata
	INS "rrdldata.dat"

scrolltext
	.byte "   "
	;      |..............................||..............................|
	.byte "22 YEARS AFTER"
	.byte $80|32
	.byte "RELEASING JOYRIDE"
	.byte $80|32
	.byte "SANDOR OF HARD"
	.byte $80|30
	.byte "PROUDLY PRESENTS"
	.byte $80|34
	.byte "REHARDEN"
	.byte $80|46
	.byte "HARDBASS"
	.byte $80|40
	.byte "HARD COLOR MAP (HCM)"
	.byte $80|32
	.byte "IN A SINGLE-FILE PRODUCTION FOR 64K PAL ^_ XL/XE"
	.byte $80|32
	.byte "THE REHARDEN LOGO ABOVE IS IN HCM MODE 0 WHICH "
	.byte "MEANS NINE COLORS ON 28-BYTE WIDTH IN ANTIC-E AT 8KB OF SIZE."
	.byte $80|64
	.byte "THE HCM SCREEN KERNEL IS SMALL AND GENERIC THEREFORE HCM PICTURES CAN BE "
	.byte "FULLY MANIPULATED AT RUN-TIME."
	.byte $80|25
	.byte "NOW LET'S HAVE A LOOK AT HCM MODE 2 WHICH IS SEVEN COLORS, IDEAL FOR GREYSCALE IMAGES"
	.byte $80|8
	.byte "SOME TECHNICAL DETAILS FOLLOW..."
	.byte $80|4
	.byte "HCM: 10 SPRITE UNDERLAY WITH TWO PLAYERS RE-POSITIONED "
	.byte "AND RE-POPULATED WITH DATA EACH SCANLINE"
	.byte $80|8
	.byte "HARDBASS: MINIMALISTIC / CHEAP TO RUN SOFTSYNTH RUNNING AT JUST 1950 SAMPLES PER SECOND "
	.byte "IN 64KHZ POKEY MODE *SHARING* CHANNEL #4 WITH RMT IN REHARDEN."
	.byte $80|40
	.byte $ff

tsscrolltext
	.byte "      GREETINGS TIME!!! *** SPECIAL THANKS TO HEAVEN AND WIECZOR FOR NAGGING ME TO CREATE SOMETHING AGAIN "
	.byte "  AND   GREY FOR SCREENING REHARDEN AND MY GREETINGS VIDEO AT SILLY VENTURE ***   "
	.byte "THANKS TO THE CREATORS OF THE WONDERFUL TOOLS I USED * PHAERON/ALTIRRA * "
	.byte "TEBE/MADS * JAC/WUDSN * RASTER/RMT * XXL/XBIOS * PLAYERMISSILE/OMNIVORE "
	.byte "*** GREETINGS TO * TQA * LAMERS * DESIRE * MAD TEAM * AGENDA * VISDOM * MEC * "
	.byte "LARESISTANCE * NOICE * MYSTIC BYTES * ATARIAGE FORUM USERS: STEPHEN * MCLANEINC * MARIUSZ * MATHY * "
	.byte "_THE DOCTOR__ * CHARLIECHAPLIN * BRYAN * "
	.byte "AND EVERYBODY ELSE ON THE ATARIAGE FORUMS * THANKS FOR KEEPING THE ATARI 8-BIT SCENE ALIVE! * "
	.byte "   SPECIAL GREETINGS TO FARKAS AND TAMAS OF HARD AND * MY SON MARTON :) *   "
	.byte "   I'M RELEASING ALL THE SOURCE CODE FOR REHARDEN / HARDBASS AND HCM SO FEEL FREE TO DIG IN / MODIFY / USE!     "
	.byte "THIS IS THE END OF THIS LITTLE DEMO. I HOPE YOU LIKED IT! *** REHARDEN WAS RELEASED AT SILLY VENTURE 2017 *** WE'RE GOING TO STAY ON THIS TWISTER-SCROLL "
	.byte "SCREEN SO THAT YOU CAN KEEP LISTENING TO THIS HARDBASS+RMT TUNE UNTIL YOU GET BASS OVERDOSE :)"
	.byte $ff

	//..

	.if *>=(PLAYER-$320)
	;address check based on advise from TeBe
	ert "Code before RMT Player got too long! Address: "*
	.else
	.print "Space left before RMT Player: ",PLAYER-$320-*," (address: ",*,")"
	.endif

	org PLAYER-$320
rmt_player_work_area
	.ds $320

	.def STEREOMODE=0
	.def HARDBASS=1
	ICL "rmtplayr.asm"
	
rmt_player_end

	org $d800
	opt h- ;RMT module is standard Atari binary file already
	ins "reharden.rmt" ;include music RMT module
	opt h+
rmt_music_data_end

	run start	;Define run address
	
	org $00
pattern_index_cursor
	.ds 2
pattern_data_cursor
	.ds 2

spsave	.ds 1
zp_tmp	.ds 2
zp_tmp2	.ds 2
zp_tmp3	.ds 2
zp_tmp4	.ds 2
zp_tmp5	.ds 2
zp_tmp6	.ds 1
zp_tmp7	.ds 2
zp_tmp8	.ds 2
zp_tmp9	.ds 1
timerena
	.byte timer
tsscrollptr
	.ds 2

xorshift_state
	.word 1
xorshift_state_tmp
	.ds 2

instrFlags .ds 1

zpvarsend