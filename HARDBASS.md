# HARDBASS

## What is HARDBASS?

HARDBASS is a minimalistic and cheap-to-run soft-synth for the Atari XL/XE computers aimed to be used in demos and possibly games.

## Getting the biggest bang for the buck

The main design principle behind HARDBASS was:

Resolve the �bass situation� on the XL/XE in a way that the resulting soft-synth is affordable to run in any production (demos and possibly games) while it still sounds good enough to give us an experience we didn�t normally have in demos/games before.

At first I implemented various low-pass filters in Java and started reducing the quality (in every sense of the word) until I noticed all benefit is lost. I went a little higher back from there and that�s HARDBASS. Giving you something you didn�t have before as cheap as possible.

In other words, HARDBASS is not a fully-blown soft-synth or SID emulator you�re used to at this stage. The range of tones is narrow (aimed at deep bass only), but it can be used in virtually any production in my view. Please note that HARDBASS is running across all screens in the REHARDEN demo without a hiccup including the HCM screens that use a 192 scanline tight screen-kernel. I think it means there�s no excuse not to use it :)

## Features and limitations of the HARDBASS sound engine

- Very low, 1950 samples per second (1 update per 8 scanlines / �char� line)
- Filter Cutoff setting (in 16 steps - most of the range is useable I�d say)
- Pulse Width setting (in 256 steps - not all sound nice due to the low update frequency; especially at higher pitch)
- Ring-modulation/sync type sounds (this is a great limitation that turned into a great feature: if you go higher with the frequency, the sound gradually translates into this metallic/robotic effect rather than a clean bass sound - I used this in REHARDEN, did a portamento from a lower pitch sound to a high pitch sound and back, quickly and I like the result of that)
- Optional timer / screen kernel based updates (see the HCM screens in REHARDEN for a mix of the two solutions on one screen)
- 64kHz timer for practical usability (using 15kHz would have been much simpler as it aligns with the scan line frequency / system clock perfectly; unfortunately you can�t switch a single POKEY channel to 15kHz and there�s plenty of reasons to keep the other channels in 64kHz - to be able to play nice drum sounds with high-pitch noise components for one - so no more sacrificing all the other channels for the sake of bass)
- Channel sharing with RMT (this functionality I actually put into RMT�s player code; search for HARDBASS in rmtplayr.asm). You can have RMT sounds and HARDBASS sounds in the same channel (channel #4 in REHARDEN). Channel multiplexing happens automatically. RMT tells HARDBASS when to mute/play and the switch works seamlessly whenever RMT plays a sound on Channel #4 or uses a joint/filtered sound on Channel #2 which requires Channel #4 to work. I for example have a mixed instrument in REHARDEN that starts with an RMT hi-hat sound and continues in a pulsewidth modulated HARDBASS sound. A quite typical requirement in chiptune bass sounds. Since both HARDBASS and RMT works in 64kHz mode, it works out naturally. There�s another benefit from channel sharing: In a typical chiptune pattern, you�d need to mix shorter bass sounds with drum sounds/etc� With RMT+HARDBASS, you can let the HARDBASS sound play long sounds not taking any future drums into account. Then your final RMT track will automatically take care of the length of the HARDBASS sounds. When a drum or any other RMT sound using Channel #4 has finished, the HARDBASS sound that got interrupted continues playing in the next frame automatically.
- The HARDBASS 64kHz IRQ needs re-alignment in every frame as the system clock is not dividable by the timer base frequency. The re-alignment be done at the start/end of your screen kernel (see the HCM screens in REHARDEN) or via DLIs (see the TwisterScroller/Greetings screen in REHARDEN) or via a VCOUNT based solution

### Design decisions:

In REHARDEN, HARDBASS runs at only 1950 samples per second. Could be higher, but like I said, that�s not my vision with HARDBASS. Feel free to modify/use it the way you like though.

Another design decision was to not include a separate volume and cutoff setting. The reasons are that low pass filtering causes decrease in volume. The stronger the filter, the lower the volume. Also, the gain from having a separate volume setting would be slim: The ability to play sharp sounds at low frequencies in contrast to deeper/filtered sounds. That�s not what I needed and therefore I couldn�t justify an extra table lookup for the sake of a volume setting. Again, if you need that, modify/use it that way in your production.

## Features and limitations of the HARDBASS tracker

The tracker is very experimental. The Java sound engine and the export functionality is fine, but the UI I just rushed together. It doesn�t have file open/save dialogs. It loads/saves/exports/imports into/from hardcoded file names. You�ll have to look at the code and experiment with it. I hope someone will rewrite the whole UI some day :)

### Features:

- Same sound generation engine as in the assembly code (same code in Java version; I�m not emulating Altirra�s non-linear mixing functionality, but it would be really neat)
- Instruments with vibrato and LFOs (very simple LFOs/vibrato implementation; didn�t find them terribly useful yet)
- Pulse Width setting (see above)
- Low-pass filter cutoff setting (see above)
- Portamento function (this one�s very useful and easy to use)
- Gate on/off column (same column as portamento; sound is only audible in a row if gate is on or the portamento downward facing arrow is set in the row; a gap in the gate doesn�t cause the portamento to reset; it keeps going so you can create cool rhythmic gaps in it this way)
- Song controls with length/loop and patterns settings (song speed/etc� is hardcoded, I didn�t take the time to add more UI for this once I settled on what I wanted for REHARDEN - feel free to add it after having a look at Song.java)
- Play from song position (Play button always plays from the actual song position; click on a pattern in the song to set the position somewhere)
- Auto-saving song into /sdcard/song1.hbass on backgrounding the app
- Auto-loading song from /sdcard/song1.hbass on launch (if exists)
- Backup button (saves the song into a newly generated backup file in /sdcard/ root - both this and the load/save above are lazy solutions instead of asking for a file name with UI)
- Export button (exports the song so that the asm code can play it; always saves into /sdcard/export.hardbass)
- Import RMT button (see the details towards the end of this document)

## Technical details and requirements:

### In the configuration REHARDEN uses HARDBASS

- Total CPU consumption per frame in REHARDEN: Approx 3300 cycles per frame on average and approx 3800 cycles per frame maximum (depending on what the tracker is playing in the frame)
- Total memory consumption including the code and bass music data in REHARDEN: Approx 2500 bytes
- See the breakdown below

### Update rate/requirement:

- 1950 samples per second (39 updates per frame => 1 update per 8 scanlines / �char�)

### HARDBASS engine (to generate the sound):

Approx CPU consumption:

- Populating the sound buffer with data: approx 1844 cycles per frame
- This code is tiny, could go on the zero page if a small increase in speed matters for your production

Updates:

- When updates are driven by a screen kernel: 14 cycles consumed per 8 scanlines / �char�
- When updates are driven by an IRQ: 54/55 cycles consumed by the IRQ per 8 scanlines / �char�
- On a typical HCM screen: 24 screen kernel based updates + 15 IRQ based updates = 336+818 = 1154 cycles per frame
- Therefore sound generation including updates totals at: 2998 cycles per frame

Approx memory consumption:

- 2*39 bytes sample buffer
- 256+16 bytes �filter map�
- 260 bytes code size (from which $1e bytes is zero-page IRQ code - not an absolute requirement, just a bit faster)
- Total: approx 610 bytes

### HARDBASS tracker player (to play music on the sound generator)

Approx CPU consumption:

This piece of code is too complex for me to calculate it instruction by instruction, but I managed to measure these values with Altirra�s history view when running REHARDEN:

- Typical: 142 cycles per frame (when no commands need to be processed in the pattern row)
- Maximum: 767 cycles per frame (when there�s an instrument change and a few things need to be set in a pattern row)

Approx memory consumption:

- $350 bytes tracker player code
- 988 bytes of music data (few patterns with notes/PW sweeps/Filter Cutoff changes/portamento exported from the experimental HARDBASS Tracker for Android)
- Total: Approx 1836 bytes

## Importing from RMT:

The Import from RMT button imports the notes from an exported RMT track text file into the actual pattern on screen. The pattern length is currently hard-coded as 64.

Settings are read from /sdcard/hardbassimport/import.properties , the track is read from /sdcard/hardbassimport/0.txt .

Example import.properties file:

```
#all numbers in decimal
sourceInstr=63
targetInstr=0
transpose=12
startCutoff=9
endCutoff=6
startPW=128
endPW=-1
pwStep=8
holdGateForSteps=255
```

- sourceInstr: The instrument to take the notes from (other instruments are ignored; see my workflow description below where I reference instrument $3f = 63)
- targetInstr: The HARDBASS instrument to use
- transpose: Add this number of semitones to the imported note (+-)
- startCutoff: The cutoff to start each note with
- endCutoff: End each note with this cutoff value
- startPW/endPW/pwStep: Not supported, I used an LFO in REHARDEN
- holdGateForSteps: How many steps/pattern rows to hold the gate for. I have the gate always on in the tracks I imported from RMT for REHARDEN, so I set it to 255.

### Advice on exporting from RMT:

I have the following workflow that handles the HARDBASS instrument that starts with an RMT hi-hat sound:

- RMT Instrument $3f = placeholder HARDBASS instrument
- Starts with 1 value of high-pitch noise (hi-hat)
- Continues with 1 value of distortion A middle-volume and loops back here (continues playing indefinitely)
- I use this instrument for all bass in channel #4 (I don�t mind it�s not a deep sound, it�ll still do the harmonies I need)
- When exporting, save your file and now do a change before export that you don�t save into the RTM song: replace the volume of the second $3f instrument value to 0 (the looping distortion A will be at volume 0 in the exported file)
- Export the stripped song
- Undo the change or quit RMT without saving

Benefits:
- You hear your harmonies complete with a simplified/higher pitched version of your bass
- When exporting, the HARDBASS instrument will still start with the hi-hat which will auto-mix into the beginning of your real HARDBASS sounds in your demo (via channel sharing)

## Vision:

I�d very much like to see a HARDBASS track integrated into RMT. It could be Channels #1/2/3/4 as normal in RMT, plus HARDBASS in its separate channel on the screen. Since HARDBASS does channel sharing with RMT on Channel #4, it�d be very intuitive to use and highly flexible for mixed channels and mixed RMT/HARDBASS instruments. I know the community doesn�t have the source code of RMT, but I�m hoping something like this can happen in the future.

A much more straightforward modification would be the inclusion of sampled sounds in HARDBASS/HARDBASS Tracker so that you could drop in digi drums into the same channel and it�d get auto-multiplexed with HARDBASS when RMT gives us green light to play any sound. I was a lot more interested to see 64kHz sounds working together with HARDBASS, so I didn�t go for this myself. ...but would make sense I�d say.

## Tracker Sources

- [HARDBASS Tracker for Android](https://bitbucket.org/sandor-hard/hardbass-tracker-android/)

## Example code:

- [HARDBASS+RMT Player Example for Atari XL/XE](https://bitbucket.org/sandor-hard/hardbass-player-example/)

Happy hacking!

Sandor Teli / HARD
