# Hard Color Map (HCM)

## What is HCM?

HCM is a homogeneous graphics format for the Atari XL/XE computers that can come in multiple flavors (called �modes�).

The common characteristic between all current and possible future HCM modes:

- PM (player/missile) color map that is achieved via static repositioning of PMs. These replica PMs also have their own data that represent a section of the color map.
- Each two attribute blocks (one byte on the screen in Antic E/F terms along with their corresponding color map section) are interchangeable on the image
- Each mode defines the required PRIOR setting (whether or not to mix the playfield colors with the color map colors and whether or not to mix the color map PM colors with each other)

## Why to use HCM?

There�s a wonderful tool called RastaConverter that gives us dozens of colors in an average picture. No HCM mode will give you that.
There�s also advanced games/engines that use PM underlays. However these have an allocation/multiplexing logic behind them making it non-homogenous meaning it�s not a graphics mode, but a dedicated program for a pre-defined job.

Consider using HCM if any of the following is crucial for your product:

- Can be manipulated at run-time in a 100% predictable way (swap any two attribute blocks in the image; do/invent transition effects, see the composition/dismantling of HCM pictures in REHARDEN)
- Generic screen kernel that leaves you a little time to perform a small task in each scanline (like updating a soft synth, see REHARDEN / HARDBASS on HCM screens)
- Small size (the HCM modes demonstrated in REHARDEN are just 8KB of size)

Note that REHARDEN is a single-file 64KB demo showing multiple pictures and other features with no compression used therefore my transition effects use little memory and much CPU. For productions aiming at expanded machines with lots of RAM, much faster / more advanced transitions are possible. (fading colors / etc�)

## HCM modes

### Demonstrated in REHARDEN

#### Mode 0

- Antic E narrow mode based
- 9 colors available across 3 overlapping color palettes (each palette is freely choosable for any Antic E byte)
- 28 byte wide color map (with another 2-2 characters on sides usable with palette #0)
- 10 PMs in color map with two Players re-positioned and re-populated with data each scanline
- PM colors mix with PF colors
- PM colors don�t mix with each other

The 9 colors are constructed as:

*Palette #0*

- colorBck
- colorPlayfield0
- colorPlayfield1
- colorPlayfield2

*Palette #1*

- colorUnderlay1
- colorPlayfield0|colorUnderlay1
- colorPlayfield1|colorUnderlay1
- colorUnderlay1

*Palette #2*

- colorUnderlay2
- colorPlayfield0
- colorPlayfield1
- colorPlayfield2 | colorUnderlay2

#### Mode 2

- Antic E narrow mode based
- 7 colors available across 4 overlapping color palettes (each palette is freely choosable for any Antic E byte)
- 28 byte wide color map (with another 2-2 characters on sides usable with palette #0)
- 10 PMs in color map with two Players re-positioned and re-populated with data each scanline
- PM colors don�t mix with PF colors
- PM colors mix with each other

The 7 colors are constructed as:

*Palette #0*

- colorBck
- colorPlayfield0
- colorPlayfield1
- colorPlayfield2

*Palette #1*

- colorUnderlay1
- colorPlayfield0
- colorPlayfield1
- colorPlayfield2

*Palette #2*

- colorUnderlay2
- colorPlayfield0
- colorPlayfield1
- colorPlayfield2

*Palette #3*

- colorUnderlay1|colorUnderlay2
- colorPlayfield0
- colorPlayfield1
- colorPlayfield2


### Modes not demonstrated, but implemented to some extent

#### Mode 4

- Antic F (hi-res) narrow mode based
- 8 colors available across 4 color palettes (each palette is freely choosable for any Antic E byte)
- 28 byte wide color map (with another 2-2 characters on sides usable with palette #0)
- 10 PMs in color map with two Players re-positioned and re-populated with data each scanline
- PM colors mix with PF colors
- PM colors mix with each other

The 8 colors are constructed as:

*Palette #0*

- colorPlayfield2
- (colorPlayfield2&0xf0)|(colorPlayfield1&0x0f)

*Palette #1*

- colorUnderlay1
- (colorUnderlay1&0xf0)|(colorPlayfield1&0x0f)

*Palette #2*

- colorUnderlay2
- (colorUnderlay2&0xf0)|(colorPlayfield1&0x0f)

*Palette #3*

- colorUnderlay1|colorUnderlay2
- ((colorUnderlay1|colorUnderlay2)&0xf0)|(colorPlayfield1&0x0f)

I didn�t find a valid/impressive use case for this very limited hi-res 8 color mode, I always ended up favoring Mode 2 in greyscale over this mode.

#### Mode 1
Interlaced variant of Mode 0

#### Mode 3
Interlaced variant of Mode 2

#### Mode 5
Interlaced variant of Mode 4

I implemented an experimental version of these interlaced modes, but due to the lack of the wide use of CRT screens today and the �unrealistically good� results given for these in emulators, I didn�t pursue this further. Feel free to if you think building out interlaced modes is still something to do today.

### Possible future modes

Looking at the amount of sub-modes the community built on top of our HIP graphics format in the past two+ decades, there�s chances the same would happen to HCM.

I think the following areas are worth exploring when thinking about adding new HCM modes:

- MORE PM replicas - if you�re smart enough to squeeze in more PM replicas re-positioned and re-populated with data to achieve a wider homogenous color map, please do and give it a new mode! Both HCM and HARDBASS was done after 22 years of not looking at 6502 code and I did the engines of both of these in the 1st two-three weeks after this hiatus in October/November of 2016. I might have not achieved everything that could have been achieved in terms of PM replication. I�m hoping I haven�t and you�ll be able to do it!
- Bringing in GTIA modes into the mix in some fashion
- Introducing char based modes using the VSCROL trick
- Whatever else you can come up with :)

## Recommendations

My recommendation for PixelArt in mode 0 (like the REHARDEN logo) is:

- Create a reference image with the HCM Converter utility (easy, see the main function)
- Draw a picture in Gimp/Photoshop or whatever you�re familiar with. Always sample the used colors from the reference image. (keep it opened) Always use the colors that are compatible with your palette/attribute block (the reference image shows all palettes and their colors in the order of brightness)
- Convert the picture with the HCM Converter in mode 0. If you drew the picture correctly, the conversion will be pixel perfect. (it was in my case for the REHARDEN logo at least)
- Issues I noticed: This only works for mode 0; The output bmp image wasn�t created for me, but the HCM was. (I�m using the marvin plugin for generating/reading images; something didn�t work out with it)

## Converter Tool

Please find the converter tool I used during the production of the REHARDEN demo at the following location:

- [HCM Converter](https://bitbucket.org/sandor-hard/hcm-converter/)

## Example code

Please find example code at the following location:

- [HCM Viewer Example code for Atari XL/XE](https://bitbucket.org/sandor-hard/hcm-viewer-example/)

Alternatively, please dig out the relevant parts from REHARDEN as I released the full source code.

I also released the source code of the HCM Converter utility for Android for you to use / modify.

Happy hacking!

Sandor Teli / HARD
