# reharden.atr

This is the original ATR release of REHARDEN for Atari 8-bit computers. (Silly Venture, 2017)

# Requirements

64K PAL Atari XL/XE (tested on stock 800XL / 130XE) or [Altirra emulator](http://www.virtualdub.org/altirra.html) (tested on 2.90).

# Usage

Boot as normal (hold Option key, switch computer on).

# Contents

The disk contains the reharden.xex and [xbios.com](http://xxl.atari.pl/) files (both renamed as part of the ATASCII directory art).

The ATASCII directory art was created in [Omnivore](http://playermissile.com/omnivore/).

Enjoy,
Sandor Teli / HARD