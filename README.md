# REHARDEN

## What is REHARDEN?

REHARDEN is a demo written for Atari 8-bit computers (64K PAL XL/XE).

Click here to see it:

- [REHARDEN on YouTube](https://youtu.be/2wb3rKKCjAw)

## What does this repo include?

This repo includes the full source code of REHARDEN. Use the [WUDSN IDE](https://www.wudsn.com/) to compile and run reharden.asm.

Please click the links below for details on the HARD Color Map graphics format and the HARDBASS soft-synth that REHARDEN showcases:

- [Hard Color Map a.k.a. HCM documentation](https://bitbucket.org/sandor-hard/reharden/src/master/HCM.md)
- [HARDBASS soft-synth documentation](https://bitbucket.org/sandor-hard/reharden/src/master/HARDBASS.md)

Find the ATR disk release at:

- [REHARDEN ATR disk release](https://bitbucket.org/sandor-hard/reharden/src/master/atr%20disk%20image/)

Sandor Teli / HARD
